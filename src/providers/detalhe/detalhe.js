var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
PouchDB.plugin(PouchDBFind);
var DetalheProvider = /** @class */ (function () {
    function DetalheProvider() {
    }
    DetalheProvider.prototype.handleChange = function (e) { };
    DetalheProvider.prototype.getProdutoDetalhe = function (produto, empresa) {
        var _this = this;
        this.db = new PouchDB(empresa);
        var data = [];
        return new Promise(function (resolve) {
            _this.db.find({
                selector: { _id: produto },
            }).then(function (result) {
                data = [];
                data = result.docs;
                resolve(data);
            }).catch(function (error) {
                console.log(error);
            });
        });
    };
    // ShowRelacionados(empresa, item, montadora, modelo, de, ate){
    // 	console.log(de);
    // 	console.log(ate);
    // 	this.db = new PouchDB(empresa);
    // 	let data = [];
    // 	return new Promise(resolve => {
    // 		this.db.find({
    // 			"selector" : {
    // 					"_id": { $ne: item},
    // 					"aplicacoes" : {
    // 						"$elemMatch" : {
    // 							"montadora": { $eq: montadora},
    // 							"modelo": { $eq: modelo},
    // 							"anos": {
    // 								$exists: [de, ate]
    // 							}
    // 						}
    // 					},
    // 				},
    // 			"sort": ['_id'],
    // 			"limit": 10
    // 		}).then((result) => {
    // 			data = [];
    // 			data = result.docs;
    // 			resolve(data);       
    // 		}).catch((error) => {
    // 			console.log(error);
    // 		});
    // 	});
    // }
    DetalheProvider.prototype.getListaPesquisa = function (empresa, item, montadora, modelo, de, ate) {
        var _this = this;
        this.db = new PouchDB(empresa);
        var total;
        return new Promise(function (resolve) {
            _this.db.info().then(function (info) {
                total = info.doc_count;
            });
            _this.db.allDocs({
                include_docs: true,
                descending: true
            })
                .then(function (result) {
                _this.data = [];
                result.rows.map(function (row) {
                    if (row.doc._id.substring(0, 8) !== '_design/') {
                        _this.data.push(row.doc);
                    }
                });
                resolve(_this.data);
                _this.db.changes({ live: true, since: 'now', include_docs: true }).on('change', function (change) {
                    _this.handleChange(change);
                });
            })
                .catch(function (error) {
                console.log('Error when getUsers!', error);
            });
        });
    };
    DetalheProvider.prototype.ShowRelacionados = function (empresa, item, termo_item, montadora, modelo, de, ate) {
        var _this = this;
        this.db = new PouchDB(empresa);
        termo_item = montadora + ' ' + modelo + ' ' + de;
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var acertos, resultado, items, index, item_1, busca, index_1, aplicacao, index_2, termo;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        termo_item = termo_item.toLowerCase();
                        termo_item = termo_item.split(' ');
                        acertos = 0;
                        resultado = [];
                        items = '';
                        if (!((termo_item == false) || (termo_item == '') || (termo_item == undefined))) return [3 /*break*/, 1];
                        return [3 /*break*/, 3];
                    case 1: return [4 /*yield*/, this.getListaPesquisa(empresa, item, montadora, modelo, de, ate)];
                    case 2:
                        items = _a.sent();
                        _a.label = 3;
                    case 3:
                        for (index = 0; index < items.length; index++) {
                            item_1 = items[index];
                            busca = " " + item_1.titulo + " " + item_1.categoria + " " + item_1.termos_clear + " ";
                            for (index_1 = 0; index_1 < item_1.aplicacoes.length; index_1++) {
                                aplicacao = item_1.aplicacoes[index_1];
                                busca += " " + aplicacao.anos + " " + aplicacao.montadora + " " + aplicacao.modelo + " ";
                            }
                            busca = busca.toLowerCase();
                            for (index_2 = 0; index_2 < termo_item.length; index_2++) {
                                termo = termo_item[index_2];
                                if (busca.includes(termo)) {
                                    acertos++;
                                }
                            }
                            if (acertos === termo_item.length)
                                resultado.push(item_1);
                            acertos = 0;
                            busca = '';
                        }
                        resolve(resultado);
                        return [2 /*return*/];
                }
            });
        }); });
    };
    DetalheProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [])
    ], DetalheProvider);
    return DetalheProvider;
}());
export { DetalheProvider };
//# sourceMappingURL=detalhe.js.map