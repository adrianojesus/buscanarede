import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find'
PouchDB.plugin(PouchDBFind);


@Injectable()
export class DetalheProvider {

	db: any;
	data: any;

	constructor() {}

	handleChange(e) {}

  	getProdutoDetalhe(produto, empresa){
  		this.db = new PouchDB(empresa);
		let data = [];
		return new Promise(resolve => {
			this.db.find({
				selector: { _id: produto },
			}).then((result) => {
				data = [];
				data = result.docs;
				resolve(data);       
			}).catch((error) => {
				console.log(error);
			});
		}); 
	}

	// ShowRelacionados(empresa, item, montadora, modelo, de, ate){
	// 	console.log(de);
	// 	console.log(ate);

	// 	this.db = new PouchDB(empresa);
	// 	let data = [];

	// 	return new Promise(resolve => {
	// 		this.db.find({
	// 			"selector" : {
	// 					"_id": { $ne: item},
	// 					"aplicacoes" : {
	// 						"$elemMatch" : {
	// 							"montadora": { $eq: montadora},
	// 							"modelo": { $eq: modelo},
	// 							"anos": {
	// 								$exists: [de, ate]
	// 							}
	// 						}
	// 					},
	// 				},
	// 			"sort": ['_id'],
	// 			"limit": 10
	// 		}).then((result) => {
	// 			data = [];
	// 			data = result.docs;
	// 			resolve(data);       
	// 		}).catch((error) => {
	// 			console.log(error);
	// 		});
	// 	});

	// }


	getListaPesquisa(empresa, item, montadora, modelo, de, ate){

		this.db = new PouchDB(empresa);

		var total;

		return new Promise(resolve => {
			this.db.info().then(function (info) {
				total = info.doc_count;
			})
			this.db.allDocs({
				include_docs: true,
				descending: true
			})
			.then((result) => {
				this.data = [];

				result.rows.map((row) => {
					if(row.doc._id.substring(0,8) !== '_design/'){
						this.data.push(row.doc);
					}
				})

				resolve(this.data);

				this.db.changes({live: true, since: 'now', include_docs: true}).on('change', (change) => {
					this.handleChange(change);
				})

			})
			.catch((error) => {
				console.log('Error when getUsers!', error);
			})

		});


	}


	ShowRelacionados(empresa, item, termo_item, montadora, modelo, de, ate) {
		
		this.db = new PouchDB(empresa);

		termo_item = montadora + ' ' +modelo + ' ' +de;
		
		return new Promise(async resolve => {

			termo_item = termo_item.toLowerCase();
			termo_item = termo_item.split(' ');

			let acertos = 0;
			let resultado = [];

			let items:any = '';
			
			if((termo_item == false) || (termo_item == '')  || (termo_item == undefined)){
				//items = await this.getLista(empresa_slug, empresa_id, total_produtos, lista, termo_item, skip, limit);
			}else{
				items = await this.getListaPesquisa(empresa, item, montadora, modelo, de, ate);
			}

			for (let index = 0; index < items.length; index++) {
				
				const item = items[index];
				let busca = ` ${item.titulo} ${item.categoria} ${item.termos_clear} `;
				for (let index = 0; index < item.aplicacoes.length; index++) {
					const aplicacao = item.aplicacoes[index];
					busca+=` ${aplicacao.anos} ${aplicacao.montadora} ${aplicacao.modelo} `
					
				}

				busca = busca.toLowerCase();

				for (let index = 0; index < termo_item.length; index++) {
					const termo = termo_item[index];
					if (busca.includes(termo)) {
						acertos++
					}
					
				}
				if (acertos === termo_item.length) resultado.push(item);

				acertos = 0;
				busca= '';
				
			}

			resolve(resultado)

		});

	}


}
