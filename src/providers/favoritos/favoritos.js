var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
PouchDB.plugin(PouchDBFind);
var FavoritosProvider = /** @class */ (function () {
    function FavoritosProvider() {
    }
    FavoritosProvider.prototype.clearUserListasList = function () {
        return new Promise(function (resolve) {
            new PouchDB('usuarios_listas').destroy().then(function () {
                resolve('complete');
            }).catch(function (err) {
            });
        });
    };
    FavoritosProvider.prototype.startUserListasList = function () {
        var _this = this;
        // BAIXA OS DADOS DA EMPRESA
        return new Promise(function (resolve) {
            _this.db_lista_user = new PouchDB('usuarios_listas');
            _this.remote = 'http://149.56.85.43:5984/usuarios_listas';
            _this.db_lista_user.sync(_this.remote).on('complete', function () {
                localStorage.setItem('favoritos', 'OK');
                resolve('complete');
            });
        });
    };
    FavoritosProvider.prototype.goUserListasList = function (empresa) {
        var _this = this;
        if (empresa == '') {
            this.db_lista_user = new PouchDB('usuarios_listas');
            var data_1 = [];
            return new Promise(function (resolve) {
                _this.db_lista_user.find({
                    selector: {
                        _id: { $gte: null },
                    },
                    sort: [{ _id: "desc" }]
                }).then(function (result) {
                    data_1 = [];
                    data_1 = result.docs;
                    resolve(data_1);
                }).catch(function (error) {
                    console.log(error);
                });
            });
        }
        else {
            this.db_lista_user = new PouchDB('usuarios_listas');
            var data_2 = [];
            return new Promise(function (resolve) {
                _this.db_lista_user.find({
                    selector: {
                        _id: empresa,
                    }
                }).then(function (result) {
                    data_2 = [];
                    data_2 = result.docs;
                    resolve(data_2);
                }).catch(function (error) {
                    console.log(error);
                });
            });
        }
        ;
    };
    ;
    FavoritosProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [])
    ], FavoritosProvider);
    return FavoritosProvider;
}());
export { FavoritosProvider };
//# sourceMappingURL=favoritos.js.map