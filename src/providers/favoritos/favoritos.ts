import { Injectable } from '@angular/core';

import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
PouchDB.plugin(PouchDBFind);

@Injectable()
export class FavoritosProvider {

	db_lista_user: any;
	remote: any;

	constructor() {}

	clearUserListasList(){
		return new Promise(resolve => {
			new PouchDB('usuarios_listas').destroy().then(function () {
				resolve('complete');
			}).catch(function (err) {
			});
		});
	}

	startUserListasList(){

		// BAIXA OS DADOS DA EMPRESA
		return new Promise(resolve => {
	
			this.db_lista_user = new PouchDB('usuarios_listas');
			this.remote = 'http://149.56.85.43:5984/usuarios_listas';
			this.db_lista_user.sync(this.remote).on('complete', function () {
				localStorage.setItem('favoritos', 'OK');
				resolve('complete');
			});

		});
	}

	goUserListasList(empresa) {

		if(empresa == ''){

			this.db_lista_user = new PouchDB('usuarios_listas');

			let data = [];
			return new Promise(resolve => {

				this.db_lista_user.find({
					selector: {
						_id: { $gte: null },
					},
					sort: [{_id: "desc"}]
				}).then((result) => {
					data = [];
					data = result.docs;
					resolve(data);
				}).catch((error) => {
					console.log(error);
				});

			}); 

		}else{

			this.db_lista_user = new PouchDB('usuarios_listas');

			let data = [];
			return new Promise(resolve => {

				this.db_lista_user.find({
					selector: {
						_id: empresa,
					}
				}).then((result) => {
					data = [];
					data = result.docs;
					resolve(data);
				}).catch((error) => {
					console.log(error);
				});

			}); 

		};


	};

}
