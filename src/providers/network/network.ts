import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { BehaviorSubject } from 'rxjs';

declare let navigator;

@Injectable()
export class NetworkProvider {


	constructor(public network: Network, public toast: ToastController) { }

	private isConnected = new BehaviorSubject<boolean>(navigator.onLine);

	// Observar este método para saber de existe conexão com a internet
	getIsOnline(): Observable<boolean> {
		return this.isConnected.asObservable();
	}

	verifyConnectionNow(){
		return navigator.onLine;
	}

	observeConnection(): void {
		this.network.onConnect().subscribe(() => {
			this.toast.create({
				message: 'Conectado a Internet',
				duration: 3000,
				position: 'center',
				cssClass: "mensagemNet",
			}).present();

			localStorage.setItem('internet', 'on');
			this.isConnected.next(true);
		});

		this.network.onDisconnect().subscribe(() => {

			this.toast.create({
				message: 'Sem acesso a Internet',
				duration: 3000,
				position: 'center',
				cssClass: "mensagemNet",
			}).present();

			localStorage.setItem('internet', 'off');
			this.isConnected.next(false);
		});
	}
}
