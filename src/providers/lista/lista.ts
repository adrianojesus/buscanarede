import { Injectable } from '@angular/core';

import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find'
PouchDB.plugin(PouchDBFind);

@Injectable()


export class ListaProvider {

	data: any;
	db: any;
	remote: any;
	filtro: '';

	constructor() {}

	handleChange(e) {
		
	}


	getProdutos(empresa_slug, empresa_id, total_produtos, lista){

		this.db = new PouchDB(empresa_slug);

		let total_fracao:number = 0;
		total_fracao = (total_produtos / 100);
		let contador:number = 0;

		return new Promise(resolve => {
			this.remote = 'http://149.56.85.43:5984/'+empresa_slug+'';

			this.db.sync(this.remote).on('change', function(change){

				this.completo = ((contador * 100) / total_fracao);

				if(this.completo > 100){
					localStorage.setItem('percentagem', '100');
					localStorage.setItem('percentagem_'+empresa_slug+'', '100');
				}else{
					localStorage.setItem('percentagem', this.completo);
					localStorage.setItem('percentagem_'+empresa_slug+'', this.completo);
				}

				if(lista == undefined){
					if(contador > 1){
						resolve('complete');
					}
				}

				contador ++;

			}).on('complete', function () {

				localStorage.setItem('percentagem', '100');
				localStorage.setItem('percentagem_'+empresa_slug+'', '100');
				localStorage.setItem('lista', 'OK');
				
				resolve('complete');

			});
		});

	}

	getLista(empresa_slug, empresa_id, total_produtos, lista, termo_item, skip, limit){

		this.db = new PouchDB(empresa_slug);

		var total;

		//VARIFICA SE O ACESSO NÃO É DAS LISTAS DE FAVORITOS
		if(lista == undefined){
		
			return new Promise(resolve => {
				this.db.info().then(function (info) {
					total = info.doc_count;
				})
				this.db.allDocs({
					include_docs: true,
					descending:true,
					limit: limit,
					skip:skip
				})
				.then((result) => {
					this.data = [];

					result.rows.map((row) => {
						if(row.doc._id.substring(0,8) !== '_design/'){
							this.data.push(row.doc);
						}
					})

					resolve(this.data);

					this.db.changes({live: true, since: 'now', include_docs: true}).on('change', (change) => {
						this.handleChange(change);
					})

				})
				.catch((error) => {

					console.log('Error when getUsers!', error);

				})

			});

		}else{

			var produtos: any;

			if((lista != false) && (lista != '')){
				produtos = lista.split(',');

				return new Promise(resolve => {

					this.db.info().then(function (info) {
						total = info.doc_count;
					})

					this.db.allDocs({
						include_docs: true,
						keys: [...produtos],
						descending:true,
						limit: limit,
						skip:skip
					})				
					.then((result) => {
						this.data = [];

						result.rows.map((row) => {
							if(row.doc._id.substring(0,8) !== '_design/'){
								this.data.push(row.doc);
							}
						})

						resolve(this.data);

						this.db.changes({live: true, since: 'now', include_docs: true}).on('change', (change) => {
							this.handleChange(change);
						})

					})
					.catch((error) => {

						console.log('Error when getUsers!', error);

					})

				});

			}else{

				return new Promise(resolve => {
					resolve();
				});

			}


		}

	}


	getListaPesquisa(empresa_slug, empresa_id, total_produto, lista, termo_item, skip, limit){

		this.db = new PouchDB(empresa_slug);

		var total;

		//VARIFICA SE O ACESSO NÃO É DAS LISTAS DE FAVORITOS
		if(lista == undefined){
		
			return new Promise(resolve => {
				this.db.info().then(function (info) {
					total = info.doc_count;
				})
				this.db.allDocs({
					include_docs: true,
					descending: true
				})
				.then((result) => {
					this.data = [];

					result.rows.map((row) => {
						if(row.doc._id.substring(0,8) !== '_design/'){
							this.data.push(row.doc);
						}
					})

					resolve(this.data);

					this.db.changes({live: true, since: 'now', include_docs: true}).on('change', (change) => {
						this.handleChange(change);
					})

				})
				.catch((error) => {
					console.log('Error when getUsers!', error);
				})

			});

		}else{

			var produtos: any;

			if((lista != false) && (lista != '')){
				produtos = lista.split(',');


				return new Promise(resolve => {
					this.db.info().then(function (info) {
						total = info.doc_count;
					})
					this.db.allDocs({
						include_docs: true,
						keys: [...produtos],
						descending: true
					})
					.then((result) => {
						this.data = [];

						result.rows.map((row) => {
							if(row.doc._id.substring(0,8) !== '_design/'){
								this.data.push(row.doc);
							}
						})

						resolve(this.data);

						this.db.changes({live: true, since: 'now', include_docs: true}).on('change', (change) => {
							this.handleChange(change);
						})

					})
					.catch((error) => {
						console.log('Error when getUsers!', error);
					})

				});


			}else{

				return new Promise(resolve => {
					resolve();
				});

			}


		}

	}


	goSearchLista(empresa_slug, empresa_id, total_produtos, lista, termo_item, skip, limit) {
		this.db = new PouchDB(empresa_slug);
		
		return new Promise(async resolve => {

			termo_item = termo_item.toLowerCase();
			termo_item = termo_item.split(' ');

			let acertos = 0;
			let resultado = [];

			let items:any = '';
			
			if((termo_item == false) || (termo_item == '')  || (termo_item == undefined)){
				items = await this.getLista(empresa_slug, empresa_id, total_produtos, lista, termo_item, skip, limit);
			}else{
				items = await this.getListaPesquisa(empresa_slug, empresa_id, total_produtos, lista, termo_item, skip, limit);
			}

			for (let index = 0; index < items.length; index++) {
				
				const item = items[index];
				let busca = ` ${item.titulo} ${item.categoria} ${item.termos_clear} ${item.tipo} ${item.status}`;
				for (let index = 0; index < item.aplicacoes.length; index++) {
					const aplicacao = item.aplicacoes[index];
					busca+=` ${aplicacao.anos} ${aplicacao.montadora} ${aplicacao.modelo} `
					
				}

				busca = busca.toLowerCase();

				for (let index = 0; index < termo_item.length; index++) {
					const termo = termo_item[index];
					if (busca.includes(termo)) {
						acertos++
					}
					
				}
				if (acertos === termo_item.length) resultado.push(item);

				acertos = 0;
				busca= '';
				
			}

			resolve(resultado)

		});

	}

}