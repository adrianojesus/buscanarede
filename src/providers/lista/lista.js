var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
PouchDB.plugin(PouchDBFind);
var ListaProvider = /** @class */ (function () {
    function ListaProvider() {
    }
    ListaProvider.prototype.handleChange = function (e) {
    };
    ListaProvider.prototype.getProdutos = function (empresa_slug, empresa_id, total_produtos, lista) {
        var _this = this;
        this.db = new PouchDB(empresa_slug);
        var total_fracao = 0;
        total_fracao = (total_produtos / 100);
        var contador = 0;
        return new Promise(function (resolve) {
            _this.remote = 'http://149.56.85.43:5984/' + empresa_slug + '';
            _this.db.sync(_this.remote).on('change', function (change) {
                this.completo = ((contador * 100) / total_fracao);
                if (this.completo > 100) {
                    localStorage.setItem('percentagem', '100');
                    localStorage.setItem('percentagem_' + empresa_slug + '', '100');
                }
                else {
                    localStorage.setItem('percentagem', this.completo);
                    localStorage.setItem('percentagem_' + empresa_slug + '', this.completo);
                }
                if (lista == undefined) {
                    if (contador > 1) {
                        resolve('complete');
                    }
                }
                contador++;
            }).on('complete', function () {
                localStorage.setItem('percentagem', '100');
                localStorage.setItem('percentagem_' + empresa_slug + '', '100');
                localStorage.setItem('lista', 'OK');
                resolve('complete');
            });
        });
    };
    ListaProvider.prototype.getLista = function (empresa_slug, empresa_id, total_produtos, lista, termo_item, skip, limit) {
        var _this = this;
        this.db = new PouchDB(empresa_slug);
        var total;
        //VARIFICA SE O ACESSO NÃO É DAS LISTAS DE FAVORITOS
        if (lista == undefined) {
            return new Promise(function (resolve) {
                _this.db.info().then(function (info) {
                    total = info.doc_count;
                });
                _this.db.allDocs({
                    include_docs: true,
                    descending: true,
                    limit: limit,
                    skip: skip
                })
                    .then(function (result) {
                    _this.data = [];
                    result.rows.map(function (row) {
                        if (row.doc._id.substring(0, 8) !== '_design/') {
                            _this.data.push(row.doc);
                        }
                    });
                    resolve(_this.data);
                    _this.db.changes({ live: true, since: 'now', include_docs: true }).on('change', function (change) {
                        _this.handleChange(change);
                    });
                })
                    .catch(function (error) {
                    console.log('Error when getUsers!', error);
                });
            });
        }
        else {
            var produtos;
            if ((lista != false) && (lista != '')) {
                produtos = lista.split(',');
                return new Promise(function (resolve) {
                    _this.db.info().then(function (info) {
                        total = info.doc_count;
                    });
                    _this.db.allDocs({
                        include_docs: true,
                        keys: produtos.slice(),
                        descending: true,
                        limit: limit,
                        skip: skip
                    })
                        .then(function (result) {
                        _this.data = [];
                        result.rows.map(function (row) {
                            if (row.doc._id.substring(0, 8) !== '_design/') {
                                _this.data.push(row.doc);
                            }
                        });
                        resolve(_this.data);
                        _this.db.changes({ live: true, since: 'now', include_docs: true }).on('change', function (change) {
                            _this.handleChange(change);
                        });
                    })
                        .catch(function (error) {
                        console.log('Error when getUsers!', error);
                    });
                });
            }
            else {
                return new Promise(function (resolve) {
                    resolve();
                });
            }
        }
    };
    ListaProvider.prototype.getListaPesquisa = function (empresa_slug, empresa_id, total_produto, lista, termo_item, skip, limit) {
        var _this = this;
        this.db = new PouchDB(empresa_slug);
        var total;
        //VARIFICA SE O ACESSO NÃO É DAS LISTAS DE FAVORITOS
        if (lista == undefined) {
            return new Promise(function (resolve) {
                _this.db.info().then(function (info) {
                    total = info.doc_count;
                });
                _this.db.allDocs({
                    include_docs: true,
                    descending: true
                })
                    .then(function (result) {
                    _this.data = [];
                    result.rows.map(function (row) {
                        if (row.doc._id.substring(0, 8) !== '_design/') {
                            _this.data.push(row.doc);
                        }
                    });
                    resolve(_this.data);
                    _this.db.changes({ live: true, since: 'now', include_docs: true }).on('change', function (change) {
                        _this.handleChange(change);
                    });
                })
                    .catch(function (error) {
                    console.log('Error when getUsers!', error);
                });
            });
        }
        else {
            var produtos;
            if ((lista != false) && (lista != '')) {
                produtos = lista.split(',');
                return new Promise(function (resolve) {
                    _this.db.info().then(function (info) {
                        total = info.doc_count;
                    });
                    _this.db.allDocs({
                        include_docs: true,
                        keys: produtos.slice(),
                        descending: true
                    })
                        .then(function (result) {
                        _this.data = [];
                        result.rows.map(function (row) {
                            if (row.doc._id.substring(0, 8) !== '_design/') {
                                _this.data.push(row.doc);
                            }
                        });
                        resolve(_this.data);
                        _this.db.changes({ live: true, since: 'now', include_docs: true }).on('change', function (change) {
                            _this.handleChange(change);
                        });
                    })
                        .catch(function (error) {
                        console.log('Error when getUsers!', error);
                    });
                });
            }
            else {
                return new Promise(function (resolve) {
                    resolve();
                });
            }
        }
    };
    ListaProvider.prototype.goSearchLista = function (empresa_slug, empresa_id, total_produtos, lista, termo_item, skip, limit) {
        var _this = this;
        this.db = new PouchDB(empresa_slug);
        return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
            var acertos, resultado, items, index, item, busca, index_1, aplicacao, index_2, termo;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        termo_item = termo_item.toLowerCase();
                        termo_item = termo_item.split(' ');
                        acertos = 0;
                        resultado = [];
                        items = '';
                        if (!((termo_item == false) || (termo_item == '') || (termo_item == undefined))) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.getLista(empresa_slug, empresa_id, total_produtos, lista, termo_item, skip, limit)];
                    case 1:
                        items = _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, this.getListaPesquisa(empresa_slug, empresa_id, total_produtos, lista, termo_item, skip, limit)];
                    case 3:
                        items = _a.sent();
                        _a.label = 4;
                    case 4:
                        for (index = 0; index < items.length; index++) {
                            item = items[index];
                            busca = " " + item.titulo + " " + item.categoria + " " + item.termos_clear + " " + item.tipo + " " + item.status;
                            for (index_1 = 0; index_1 < item.aplicacoes.length; index_1++) {
                                aplicacao = item.aplicacoes[index_1];
                                busca += " " + aplicacao.anos + " " + aplicacao.montadora + " " + aplicacao.modelo + " ";
                            }
                            busca = busca.toLowerCase();
                            for (index_2 = 0; index_2 < termo_item.length; index_2++) {
                                termo = termo_item[index_2];
                                if (busca.includes(termo)) {
                                    acertos++;
                                }
                            }
                            if (acertos === termo_item.length)
                                resultado.push(item);
                            acertos = 0;
                            busca = '';
                        }
                        resolve(resultado);
                        return [2 /*return*/];
                }
            });
        }); });
    };
    ListaProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [])
    ], ListaProvider);
    return ListaProvider;
}());
export { ListaProvider };
// export class ListaProvider {
// 	data: any;
// 	db: any;
// 	remote: any;
// 	filtro: '';
// 	constructor() {}
// 	getProdutos(empresa_slug, empresa_id, total_produtos, lista){
// 		this.db = new PouchDB(empresa_slug);
// 		let total_fracao:number = 0;
// 		total_fracao = (total_produtos / 100);
// 		let contador:number = 0;
// 		return new Promise(resolve => {
// 			this.remote = 'http://149.56.85.43:5984/'+empresa_slug+'';
// 			this.db.sync(this.remote).on('change', function(change){
// 				this.completo = ((contador * 100) / total_fracao);
// 				if(this.completo > 100){
// 					localStorage.setItem('percentagem', '100');
// 				}else{
// 					localStorage.setItem('percentagem', this.completo);
// 				}
// 				if(lista == undefined){
// 					if(contador > 1){
// 						resolve('complete');
// 					}
// 				}
// 				contador ++;
// 			}).on('complete', function () {
// 				localStorage.setItem('percentagem', '100');
// 				localStorage.setItem('lista', 'OK');
// 				resolve('complete');
// 			});
// 		});
// 	}
// 	getLista(empresa_slug, empresa_id, lista){
// 		//VARIFICA SE O ACESSO NÃO É DAS LISTAS DE FAVORITOS
// 		if(lista == undefined){
// 			this.db = new PouchDB(empresa_slug);
// 			let data = [];
// 			return new Promise(resolve => {
// 				this.db.find({
// 					selector: {
// 						empresa: empresa_id,
// 					},
//   					limit : 15,
// 					sort: [{_id: 'desc'}]
// 				}).then((result) => {
// 					data = [];
// 					data = result.docs;
// 					resolve(data);       
// 				}).catch((error) => {
// 					console.log(error);
// 				});
// 			});
// 		}else{
// 			var produtos = lista.split(',');
// 			this.db = new PouchDB(empresa_slug);
// 			let data = [];
// 			return new Promise(resolve => {
// 				this.db.find({
// 					selector: {
// 						_id: {
// 			       			$in: [...produtos]
// 			    		},
// 						empresa: empresa_id,
// 					},
// 					limit: 15,
// 					sort: [{_id: 'desc'}]
// 				}).then((result) => {
// 					data = [];
// 					data = result.docs;
// 					resolve(data);       
// 				}).catch((error) => {
// 					console.log(error);
// 				});
// 			});
// 		}
// 	}
// 	getListaPesquisa(empresa_slug, empresa_id){
// 		this.db = new PouchDB(empresa_slug);
// 		let data = [];
// 		return new Promise(resolve => {
// 			this.db.find({
// 				selector: { 
// 					empresa: empresa_id,
// 				},
// 				sort: [{_id: 'desc'}]
// 			}).then((result) => {
// 				data = [];
// 				data = result.docs;
// 				resolve(data);       
// 			}).catch((error) => {
// 				console.log(error);
// 			});
// 		});
// 	}
// 	goSearchLista(empresa_slug, empresa_id, termo_item, lista) {
// 		this.db = new PouchDB(empresa_slug);
// 		return new Promise(async resolve => {
// 			termo_item = termo_item.toLowerCase();
// 			termo_item = termo_item.split(' ');
// 			let acertos = 0;
// 			let resultado = [];
// 			let items:any = '';
// 			if(termo_item == ''){
// 				items = await this.getLista(empresa_slug, empresa_id, lista);
// 			}else{
// 				items = await this.getListaPesquisa(empresa_slug, empresa_id);
// 			}
// 			for (let index = 0; index < items.length; index++) {
// 				const item = items[index];
// 				let busca = ` ${item.titulo} ${item.categoria} ${item.termos_clear} `;
// 				for (let index = 0; index < item.aplicacoes.length; index++) {
// 					const aplicacao = item.aplicacoes[index];
// 					busca+=` ${aplicacao.anos} ${aplicacao.montadora} ${aplicacao.modelo} `
// 				}
// 				busca = busca.toLowerCase();
// 				for (let index = 0; index < termo_item.length; index++) {
// 					const termo = termo_item[index];
// 					if (busca.includes(termo)) {
// 						acertos++
// 					}
// 				}
// 				if (acertos === termo_item.length) resultado.push(item);
// 				acertos = 0;
// 				busca= '';
// 			}
// 			resolve(resultado)
// 		});
// 	}
// }
//# sourceMappingURL=lista.js.map