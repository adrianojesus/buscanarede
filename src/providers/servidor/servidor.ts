import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ServidorProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServidorProvider {

	private login_url: string = 'https://www.gs1.com.br/api/';

	private oringPathImages = "https://www.buscanarede.com.br/cms/";

	constructor(public http: HttpClient) {}

	urlGet(){
		return this.login_url;
	}

	getOriginPathImgs(){
		return this.oringPathImages;
	}



}
