import { Injectable } from '@angular/core';

import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
PouchDB.plugin(PouchDBFind);

@Injectable()
export class CategoriaProvider {

	db: any;

	constructor() {

		this.db = new PouchDB('empresas');

	}


	getGategoriaLista(categoria) {

		let data = [];
		return new Promise(resolve => {

			this.db.find({
				selector: {
					_id: { $gte: null },
					categorias: { $regex: RegExp('.*?'+categoria+'.*?', 'i') }
				},
				sort: [{_id: "asc"}]
			}).then((result) => {
				data = [];
				data = result.docs;
				resolve(data);       
			}).catch((error) => {
				console.log(error);
			});

		});

	}

	goSearchEmpresa(categoria, termo) {

		let data = [];
		return new Promise(resolve => {

			this.db.find({
				"selector" : {
					"_id": { $gte: null },
					"categorias": { $regex: RegExp('.*?'+categoria+'.*?', 'i') },
					"nome": { $regex: RegExp('.*?'+termo+'.*?', 'i') }
				},
				sort: [{_id: "desc"}]
			}).then((result) => {
				data = [];
				data = result.docs;
				resolve(data);       
			}).catch((error) => {
				console.log(error);
			});

		}); 


	}

}
