var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
PouchDB.plugin(PouchDBFind);
var CategoriaProvider = /** @class */ (function () {
    function CategoriaProvider() {
        this.db = new PouchDB('empresas');
    }
    CategoriaProvider.prototype.getGategoriaLista = function (categoria) {
        var _this = this;
        var data = [];
        return new Promise(function (resolve) {
            _this.db.find({
                selector: {
                    _id: { $gte: null },
                    categorias: { $regex: RegExp('.*?' + categoria + '.*?', 'i') }
                },
                sort: [{ _id: "asc" }]
            }).then(function (result) {
                data = [];
                data = result.docs;
                resolve(data);
            }).catch(function (error) {
                console.log(error);
            });
        });
    };
    CategoriaProvider.prototype.goSearchEmpresa = function (categoria, termo) {
        var _this = this;
        var data = [];
        return new Promise(function (resolve) {
            _this.db.find({
                "selector": {
                    "_id": { $gte: null },
                    "categorias": { $regex: RegExp('.*?' + categoria + '.*?', 'i') },
                    "nome": { $regex: RegExp('.*?' + termo + '.*?', 'i') }
                },
                sort: [{ _id: "desc" }]
            }).then(function (result) {
                data = [];
                data = result.docs;
                resolve(data);
            }).catch(function (error) {
                console.log(error);
            });
        });
    };
    CategoriaProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [])
    ], CategoriaProvider);
    return CategoriaProvider;
}());
export { CategoriaProvider };
//# sourceMappingURL=categoria.js.map