import { Injectable } from '@angular/core';

import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
PouchDB.plugin(PouchDBFind);


@Injectable()
export class HomeProvider {

	data: any;
	db_cat: any;
	db_cat_user: any;
	db: any;
	db2: any;
	db3: any;
	db4: any;
	db_produtos: any;
	remote: any;
	remote_produtos: any;
	completo: any;

	percentagem: any;

	constructor() {
		this.db = new PouchDB('empresas');
	}

	getStart(){

		// BAIXA OS DADOS DA EMPRESA
		return new Promise(resolve => {
	
			this.db_cat_user = new PouchDB('usuarios_catalogos');
			this.remote = 'http://149.56.85.43:5984/usuarios_catalogos';
			this.db_cat_user.sync(this.remote).on('complete', function () {
				localStorage.setItem('userListCat', 'OK');
			});

			this.db_cat = new PouchDB('empresas_categorias');
			this.remote = 'http://149.56.85.43:5984/empresas_categorias';
			this.db_cat.sync(this.remote).on('complete', function () {
				localStorage.setItem('lista', 'OK');
			});
			

			this.remote = 'http://149.56.85.43:5984/empresas';
			this.db.sync(this.remote).on('complete', function () {
				resolve('complete');
				localStorage.setItem('start', 'OK');
			});
	

		});
	}

	goUserCatalogoList() {

		this.db_cat_user = new PouchDB('usuarios_catalogos');

		let data = [];
		return new Promise(resolve => {

			this.db_cat_user.find({
				selector: {
					_id: { $gte: null },
				},
				sort: [{_id: "asc"}]
			}).then((result) => {
				data = [];
				data = result.docs;
				resolve(data);       
			}).catch((error) => {
				console.log(error);
			});

		}); 

	}

	goCategorias() {

		this.db_cat = new PouchDB('empresas_categorias');

		let data = [];
		return new Promise(resolve => {

			this.db_cat.find({
				selector: {
					_id: { $gte: null },
				},
				sort: [{_id: "asc"}]
			}).then((result) => {
				data = [];
				data = result.docs;
				resolve(data);       
			}).catch((error) => {
				console.log(error);
			});

		}); 


	}

	getHome(){

		// PEGA A LISTA DE EMPRESAS JÁ EXISTENTE
		let data = [];
		return new Promise(resolve => {
			this.db.find({
				selector: {
					_id: { $gte: null },
					$or: [{ "visivel": "1" }]
				},
				sort: [{_id: "desc"}]
			}).then((result) => {
				data = [];
				data = result.docs;
				resolve(data);       
			}).catch((error) => {
				console.log(error);
			});
		}); 

	}

	getEmpresaLista(empresa){

		// PEGA A LISTA DE EMPRESAS JÁ EXISTENTE
		let data = [];
		return new Promise(resolve => {
			this.db.find({
				selector: { _id: empresa },
				sort: [{_id: "desc"}]
			}).then((result) => {
				data = [];
				data = result.docs;
				resolve(data);       
			}).catch((error) => {
				console.log(error);
			});
		}); 

	}

	goSearchEmpresa(termo) {

		let data = [];
		return new Promise(resolve => {

			this.db.find({
				"selector" : {
					"_id": { $gte: null },
					"nome": { $regex: RegExp('.*?'+termo+'.*?', 'i') },
					"visivel": "1"
				},
				sort: [{_id: "desc"}]
			}).then((result) => {
				data = [];
				data = result.docs;
				resolve(data);       
			}).catch((error) => {
				console.log(error);
			});

		}); 


	}


}