var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
PouchDB.plugin(PouchDBFind);
var HomeProvider = /** @class */ (function () {
    function HomeProvider() {
        this.db = new PouchDB('empresas');
    }
    HomeProvider.prototype.getStart = function () {
        var _this = this;
        // BAIXA OS DADOS DA EMPRESA
        return new Promise(function (resolve) {
            _this.db_cat_user = new PouchDB('usuarios_catalogos');
            _this.remote = 'http://149.56.85.43:5984/usuarios_catalogos';
            _this.db_cat_user.sync(_this.remote).on('complete', function () {
                localStorage.setItem('userListCat', 'OK');
            });
            _this.db_cat = new PouchDB('empresas_categorias');
            _this.remote = 'http://149.56.85.43:5984/empresas_categorias';
            _this.db_cat.sync(_this.remote).on('complete', function () {
                localStorage.setItem('lista', 'OK');
            });
            _this.remote = 'http://149.56.85.43:5984/empresas';
            _this.db.sync(_this.remote).on('complete', function () {
                resolve('complete');
                localStorage.setItem('start', 'OK');
            });
        });
    };
    HomeProvider.prototype.goUserCatalogoList = function () {
        var _this = this;
        this.db_cat_user = new PouchDB('usuarios_catalogos');
        var data = [];
        return new Promise(function (resolve) {
            _this.db_cat_user.find({
                selector: {
                    _id: { $gte: null },
                },
                sort: [{ _id: "asc" }]
            }).then(function (result) {
                data = [];
                data = result.docs;
                resolve(data);
            }).catch(function (error) {
                console.log(error);
            });
        });
    };
    HomeProvider.prototype.goCategorias = function () {
        var _this = this;
        this.db_cat = new PouchDB('empresas_categorias');
        var data = [];
        return new Promise(function (resolve) {
            _this.db_cat.find({
                selector: {
                    _id: { $gte: null },
                },
                sort: [{ _id: "asc" }]
            }).then(function (result) {
                data = [];
                data = result.docs;
                resolve(data);
            }).catch(function (error) {
                console.log(error);
            });
        });
    };
    HomeProvider.prototype.getHome = function () {
        var _this = this;
        // PEGA A LISTA DE EMPRESAS JÁ EXISTENTE
        var data = [];
        return new Promise(function (resolve) {
            _this.db.find({
                selector: {
                    _id: { $gte: null },
                    $or: [{ "visivel": "1" }]
                },
                sort: [{ _id: "desc" }]
            }).then(function (result) {
                data = [];
                data = result.docs;
                resolve(data);
            }).catch(function (error) {
                console.log(error);
            });
        });
    };
    HomeProvider.prototype.getEmpresaLista = function (empresa) {
        var _this = this;
        // PEGA A LISTA DE EMPRESAS JÁ EXISTENTE
        var data = [];
        return new Promise(function (resolve) {
            _this.db.find({
                selector: { _id: empresa },
                sort: [{ _id: "desc" }]
            }).then(function (result) {
                data = [];
                data = result.docs;
                resolve(data);
            }).catch(function (error) {
                console.log(error);
            });
        });
    };
    HomeProvider.prototype.goSearchEmpresa = function (termo) {
        var _this = this;
        var data = [];
        return new Promise(function (resolve) {
            _this.db.find({
                "selector": {
                    "_id": { $gte: null },
                    "nome": { $regex: RegExp('.*?' + termo + '.*?', 'i') },
                    "visivel": "1"
                },
                sort: [{ _id: "desc" }]
            }).then(function (result) {
                data = [];
                data = result.docs;
                resolve(data);
            }).catch(function (error) {
                console.log(error);
            });
        });
    };
    HomeProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [])
    ], HomeProvider);
    return HomeProvider;
}());
export { HomeProvider };
//# sourceMappingURL=home.js.map