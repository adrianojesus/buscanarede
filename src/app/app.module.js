var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule, ErrorHandler } from '@angular/core';
import { Keyboard } from '@ionic-native/keyboard';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { ListaPage } from '../pages/lista/lista';
import { DetalhePage } from '../pages/detalhe/detalhe';
import { FavoritosPage } from '../pages/favoritos/favoritos';
import { PerfilPage } from '../pages/perfil/perfil';
import { CategoriaPage } from '../pages/categoria/categoria';
import { NewlistPage } from '../pages/newlist/newlist';
import { NotificationPage } from '../pages/notification/notification';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomeProvider } from '../providers/home/home';
import { ListaProvider } from '../providers/lista/lista';
import { DetalheProvider } from '../providers/detalhe/detalhe';
import { ServidorProvider } from '../providers/servidor/servidor';
import { ScrollHideDirective } from '../directives/scroll-hide/scroll-hide';
import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { CategoriaProvider } from '../providers/categoria/categoria';
import { LoginProvider } from '../providers/login/login';
import { FavoritosProvider } from '../providers/favoritos/favoritos';
import { PerfilProvider } from '../providers/perfil/perfil';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                MyApp,
                FavoritosPage,
                PerfilPage,
                LoginPage,
                HomePage,
                ListaPage,
                DetalhePage,
                CategoriaPage,
                NewlistPage,
                NotificationPage,
                ScrollHideDirective,
            ],
            imports: [
                BrowserModule,
                IonicModule.forRoot(MyApp),
                HttpClientModule,
                HttpModule,
            ],
            bootstrap: [IonicApp],
            entryComponents: [
                MyApp,
                FavoritosPage,
                PerfilPage,
                LoginPage,
                HomePage,
                ListaPage,
                DetalhePage,
                CategoriaPage,
                NotificationPage,
                NewlistPage,
            ],
            providers: [
                StatusBar,
                SplashScreen,
                { provide: ErrorHandler, useClass: IonicErrorHandler },
                Keyboard,
                Facebook,
                GooglePlus,
                HomeProvider,
                ListaProvider,
                DetalheProvider,
                ServidorProvider,
                CategoriaProvider,
                LoginProvider,
                FavoritosProvider,
                PerfilProvider
            ]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map