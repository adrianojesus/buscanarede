import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { map } from 'rxjs/operators';
import { Http } from '@angular/http';

import { ServidorProvider } from '../../providers/servidor/servidor';
import { HomeProvider } from '../../providers/home/home';
import { FavoritosProvider } from '../../providers/favoritos/favoritos';

import { FavoritosPage } from '../favoritos/favoritos';

@IonicPage()
@Component({
  selector: 'page-newlist',
  templateUrl: 'newlist.html',
})

export class NewlistPage {

	loadpage: any;

	nomelista: string;
	empresalista: string;
	publica: any;

	userlistcat: any;
	userlistcattoal: any;

	userlista: any;
	userlistattoal: any;

	constructor(public navCtrl: NavController,  public HomeProvider: HomeProvider, public FavoritosProvider: FavoritosProvider, public alertCtrl: AlertController, public ServidorProvider: ServidorProvider, public http: Http, public navParams: NavParams) {}

	ionViewDidLoad() {
		//PEGA A LISTA DE CATALOGOS DO USUARIO LOGADO
		this.loadpage = true;
		this.HomeProvider.goUserCatalogoList().then((data) => {
			this.userlistcat = data;
			this.userlistcattoal = this.userlistcat.length;
		});


	}


	novalista(){

		this.loadpage = false;

		if(this.nomelista == undefined || this.empresalista == undefined){

			this.loadpage = true;

			let alert = this.alertCtrl.create({
				title: 'Atenção',
				message: 'Preencha todos os campos',
				buttons: ['OK']
			})
			alert.present();



		}else{

			if(this.publica == true){
				this.publica = 1;
			}else{
				this.publica = 0;
			}

			let user = localStorage.getItem('usuario_id');

			this.http.get(this.ServidorProvider.urlGet()+'usuarios_nova_lista.php?nomelista='+this.nomelista+'&empresalista='+this.empresalista+'&publica='+this.publica+'&user='+user+'').pipe( map( res => res.json() ) )
			.subscribe(
				dados => {
					if(dados.msg.salvo == 'sim'){

						this.getUserListasAtulizar(localStorage.getItem('usuario_id'));





					}else{
						let alert = this.alertCtrl.create({
							title: 'Atenção',
							message: dados.msg.texto,
							buttons: ['OK']
						})
						alert.present();
					}
				}
			)

		}

	}



		//ATUALIZA A LISTA DE LISTAS DO USUARIO LOGADO
	getUserListasAtulizar(user){
		this.FavoritosProvider.clearUserListasList().then((data) => {
			localStorage.setItem('FavoritosPendente', 'YES');
			this.startUserListasList();
		});
	}

	startUserListasList(){

		this.FavoritosProvider.startUserListasList().then((data) => {
			this.getUserListasList();
		});

	}

	getUserListasList(){

		this.FavoritosProvider.goUserListasList('').then((data) => {
			this.loadpage = true;
			this.userlista = data;
			this.userlistattoal = this.userlista.length;
			localStorage.setItem('FavoritosPendente', 'NO');
			this.navCtrl.setRoot(FavoritosPage);
		});

	}



  	voltar(){
		this.navCtrl.pop();
	}

}
