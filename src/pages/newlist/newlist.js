var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { map } from 'rxjs/operators';
import { Http } from '@angular/http';
import { ServidorProvider } from '../../providers/servidor/servidor';
import { HomeProvider } from '../../providers/home/home';
import { FavoritosProvider } from '../../providers/favoritos/favoritos';
import { FavoritosPage } from '../favoritos/favoritos';
var NewlistPage = /** @class */ (function () {
    function NewlistPage(navCtrl, HomeProvider, FavoritosProvider, alertCtrl, ServidorProvider, http, navParams) {
        this.navCtrl = navCtrl;
        this.HomeProvider = HomeProvider;
        this.FavoritosProvider = FavoritosProvider;
        this.alertCtrl = alertCtrl;
        this.ServidorProvider = ServidorProvider;
        this.http = http;
        this.navParams = navParams;
    }
    NewlistPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        //PEGA A LISTA DE CATALOGOS DO USUARIO LOGADO
        this.loadpage = true;
        this.HomeProvider.goUserCatalogoList().then(function (data) {
            _this.userlistcat = data;
            _this.userlistcattoal = _this.userlistcat.length;
        });
    };
    NewlistPage.prototype.novalista = function () {
        var _this = this;
        this.loadpage = false;
        if (this.nomelista == undefined || this.empresalista == undefined) {
            this.loadpage = true;
            var alert_1 = this.alertCtrl.create({
                title: 'Atenção',
                message: 'Preencha todos os campos',
                buttons: ['OK']
            });
            alert_1.present();
        }
        else {
            if (this.publica == true) {
                this.publica = 1;
            }
            else {
                this.publica = 0;
            }
            var user = localStorage.getItem('usuario_id');
            this.http.get(this.ServidorProvider.urlGet() + 'usuarios_nova_lista.php?nomelista=' + this.nomelista + '&empresalista=' + this.empresalista + '&publica=' + this.publica + '&user=' + user + '').pipe(map(function (res) { return res.json(); }))
                .subscribe(function (dados) {
                if (dados.msg.salvo == 'sim') {
                    _this.getUserListasAtulizar(localStorage.getItem('usuario_id'));
                }
                else {
                    var alert_2 = _this.alertCtrl.create({
                        title: 'Atenção',
                        message: dados.msg.texto,
                        buttons: ['OK']
                    });
                    alert_2.present();
                }
            });
        }
    };
    //ATUALIZA A LISTA DE LISTAS DO USUARIO LOGADO
    NewlistPage.prototype.getUserListasAtulizar = function (user) {
        var _this = this;
        this.FavoritosProvider.clearUserListasList().then(function (data) {
            localStorage.setItem('FavoritosPendente', 'YES');
            _this.startUserListasList();
        });
    };
    NewlistPage.prototype.startUserListasList = function () {
        var _this = this;
        this.FavoritosProvider.startUserListasList().then(function (data) {
            _this.getUserListasList();
        });
    };
    NewlistPage.prototype.getUserListasList = function () {
        var _this = this;
        this.FavoritosProvider.goUserListasList('').then(function (data) {
            _this.loadpage = true;
            _this.userlista = data;
            _this.userlistattoal = _this.userlista.length;
            localStorage.setItem('FavoritosPendente', 'NO');
            _this.navCtrl.setRoot(FavoritosPage);
        });
    };
    NewlistPage.prototype.voltar = function () {
        this.navCtrl.pop();
    };
    NewlistPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-newlist',
            templateUrl: 'newlist.html',
        }),
        __metadata("design:paramtypes", [NavController, HomeProvider, FavoritosProvider, AlertController, ServidorProvider, Http, NavParams])
    ], NewlistPage);
    return NewlistPage;
}());
export { NewlistPage };
//# sourceMappingURL=newlist.js.map