var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomeProvider } from '../../providers/home/home';
import { DetalhePage } from '../detalhe/detalhe';
var NotificationPage = /** @class */ (function () {
    function NotificationPage(navCtrl, HomeProvider, navParams) {
        this.navCtrl = navCtrl;
        this.HomeProvider = HomeProvider;
        this.navParams = navParams;
        this.headerScrollConfig = { cssProperty: 'margin-top', maxValue: 36 };
        this.footerScrollConfig = { cssProperty: 'margin-bottom', maxValue: 50 };
        this.empresa_id = this.navParams.get('empresa_id');
        this.empresa_slug = this.navParams.get('empresa_slug');
        this.empresa_nome = this.navParams.get('empresa_nome');
        this.cor = this.navParams.get('cor');
    }
    NotificationPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        //ELE PEGA A LISTA JA BAIXADA
        this.HomeProvider.getEmpresaLista(this.empresa_id).then(function (data) {
            _this.empresa_dados = data;
        });
    };
    NotificationPage.prototype.goDetalhe = function (produto) {
        this.navCtrl.push(DetalhePage, { produto: produto, empresa_slug: this.empresa_slug, empresa_nome: this.empresa_nome, cor: this.cor });
    };
    NotificationPage.prototype.voltar = function () {
        this.navCtrl.pop();
    };
    NotificationPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-notification',
            templateUrl: 'notification.html',
        }),
        __metadata("design:paramtypes", [NavController, HomeProvider, NavParams])
    ], NotificationPage);
    return NotificationPage;
}());
export { NotificationPage };
//# sourceMappingURL=notification.js.map