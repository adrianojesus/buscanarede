import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ScrollHideConfig } from '../../directives/scroll-hide/scroll-hide';

import { HomeProvider } from '../../providers/home/home'; 

import { DetalhePage } from '../detalhe/detalhe';

@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {

	headerScrollConfig: ScrollHideConfig = { cssProperty: 'margin-top', maxValue: 36 };
	footerScrollConfig: ScrollHideConfig = { cssProperty: 'margin-bottom', maxValue: 50 };

	empresa_dados: any;
	lista: any;
	empresa_id: any;
	empresa_nome: any;
	empresa_slug: any;
	cor: any;

	constructor(public navCtrl: NavController, public HomeProvider: HomeProvider, public navParams: NavParams) {
		this.empresa_id = this.navParams.get('empresa_id');
		this.empresa_slug = this.navParams.get('empresa_slug');
		this.empresa_nome = this.navParams.get('empresa_nome');
		this.cor = this.navParams.get('cor');
	}

	ionViewDidLoad() {
		//ELE PEGA A LISTA JA BAIXADA
		this.HomeProvider.getEmpresaLista(this.empresa_id).then((data) => {
			this.empresa_dados = data;
		});
	}

	goDetalhe(produto: any){
		this.navCtrl.push(DetalhePage, {produto: produto, empresa_slug: this.empresa_slug, empresa_nome: this.empresa_nome, cor: this.cor});
	}

	voltar(){
		this.navCtrl.pop();
	}

}
