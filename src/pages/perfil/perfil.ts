import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HomePage } from '../home/home';
import { FavoritosPage } from '../favoritos/favoritos';

import { ScrollHideConfig } from '../../directives/scroll-hide/scroll-hide';

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

	avatar: any;

	headerScrollConfig: ScrollHideConfig = { cssProperty: 'margin-top', maxValue: 56 };
	footerScrollConfig: ScrollHideConfig = { cssProperty: 'margin-bottom', maxValue: 56 };

	constructor(public navCtrl: NavController, public navParams: NavParams) {
	}

	ionViewDidLoad() {
		this.avatar = localStorage.getItem('usuario_avatar');
		console.log(this.avatar);
	}

	//NEVEGAÇÃO FOOTER
	goFooterLHomePage() {this.navCtrl.setRoot(HomePage);};
	goFooterFavoritosPage() {this.navCtrl.push(FavoritosPage);};
	goFooterPerfilPage() {this.navCtrl.push(PerfilPage);};

	voltar(){
		this.navCtrl.pop();
	}

}
