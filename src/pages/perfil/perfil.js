var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { FavoritosPage } from '../favoritos/favoritos';
var PerfilPage = /** @class */ (function () {
    function PerfilPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.headerScrollConfig = { cssProperty: 'margin-top', maxValue: 56 };
        this.footerScrollConfig = { cssProperty: 'margin-bottom', maxValue: 56 };
    }
    PerfilPage_1 = PerfilPage;
    PerfilPage.prototype.ionViewDidLoad = function () {
        this.avatar = localStorage.getItem('usuario_avatar');
        console.log(this.avatar);
    };
    //NEVEGAÇÃO FOOTER
    PerfilPage.prototype.goFooterLHomePage = function () { this.navCtrl.setRoot(HomePage); };
    ;
    PerfilPage.prototype.goFooterFavoritosPage = function () { this.navCtrl.push(FavoritosPage); };
    ;
    PerfilPage.prototype.goFooterPerfilPage = function () { this.navCtrl.push(PerfilPage_1); };
    ;
    PerfilPage.prototype.voltar = function () {
        this.navCtrl.pop();
    };
    var PerfilPage_1;
    PerfilPage = PerfilPage_1 = __decorate([
        IonicPage(),
        Component({
            selector: 'page-perfil',
            templateUrl: 'perfil.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], PerfilPage);
    return PerfilPage;
}());
export { PerfilPage };
//# sourceMappingURL=perfil.js.map