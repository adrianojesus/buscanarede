var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, Platform } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';
import { ListaPage } from '../lista/lista';
import { FavoritosPage } from '../favoritos/favoritos';
import { PerfilPage } from '../perfil/perfil';
import { CategoriaPage } from '../../pages/categoria/categoria';
import { HomeProvider } from '../../providers/home/home';
import { ServidorProvider } from '../../providers/servidor/servidor';
import { Observable } from 'rxjs/Rx';
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, file, HomeProvider, ServidorProvider, http, alertCtrl, loadingCtrl, platform) {
        this.navCtrl = navCtrl;
        this.file = file;
        this.HomeProvider = HomeProvider;
        this.ServidorProvider = ServidorProvider;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.categoriasLista = [];
        this.progressLista = [];
        this.listaslide = [];
        this.slidesCatalogo = 2.3;
        this.progress = 0;
        this.progress_valor = 0;
        this.anArray = [];
        this.headerScrollConfig = { cssProperty: 'margin-top', maxValue: 56 };
        this.footerScrollConfig = { cssProperty: 'margin-bottom', maxValue: 56 };
    }
    HomePage_1 = HomePage;
    HomePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.loadpage = false;
        if (localStorage.getItem('start') == 'OK') {
            //ELE PEGA A LISTA JA BAIXADA
            this.HomeProvider.getHome().then(function (data) {
                _this.itens = data;
                _this.goCategorias();
                //PEGA A LISTA DE CATALOGOS DO USUARIO LOGADO
                _this.getUserCatList();
                _this.loadpage = true;
            });
        }
        else {
            // INCIA O APP PELA PRIMEIRA VEZ ELE BAIXA AS EMPRESAS
            this.HomeProvider.getStart().then(function (data) {
                _this.HomeProvider.getHome().then(function (data) {
                    //ATUALIZA A LISTA DE CATALOGOS DO USUARIO LOGADO
                    _this.getUserCatalogoAtulizar(localStorage.getItem('usuario_id'));
                    _this.itens = data;
                    _this.goCategorias();
                    _this.loadpage = true;
                });
            });
        }
        if (this.platform.width() > 1200) {
            this.slidesCatalogo = 6.3;
        }
        else if (this.platform.width() > 768) {
            this.slidesCatalogo = 5.3;
        }
        else if (this.platform.width() > 400) {
            this.slidesCatalogo = 3.3;
        }
        else if (this.platform.width() > 319) {
            this.slidesCatalogo = 2.3;
        }
    };
    HomePage.prototype.checkFile = function (arquivo) {
        this.file.checkFile(this.file.applicationDirectory + 'www/assets/empresa/', arquivo).then(function (entry) {
            // const alert = this.alertCtrl.create({
            //   title: 'Verifica',
            //   subTitle: 'Existe',
            //   buttons: ['OK']
            // });
            // alert.present();
        }).catch(function (err) {
            // const alert = this.alertCtrl.create({
            //   title: 'Verifica',
            //   subTitle: 'Não Existe '+err+'',
            //   buttons: ['OK']
            // });
            // alert.present();
        });
    };
    //PEGA A LISTA DE CATEGORIA
    HomePage.prototype.goCategorias = function () {
        var _this = this;
        this.HomeProvider.goCategorias().then(function (data) {
            _this.categorias = data;
            for (var cat = 0; cat < _this.categorias.length; ++cat) {
                for (var emp = 0; emp < _this.categorias[cat].empresas.length; ++emp) {
                    console.log(localStorage.getItem('percentagem_' + _this.categorias[cat].empresas[emp].slug + ''));
                    if ((localStorage.getItem('percentagem_' + _this.categorias[cat].empresas[emp].slug + '') != undefined) || (localStorage.getItem('percentagem_' + _this.categorias[cat].empresas[emp].slug + '') != null)) {
                        _this.progress_valor = localStorage.getItem('percentagem_' + _this.categorias[cat].empresas[emp].slug + '');
                    }
                    else {
                        _this.progress_valor = '0';
                    }
                    _this.progressLista.push({
                        "empresa": _this.categorias[cat].empresas[emp].slug,
                        "valor": _this.progress_valor
                    });
                }
            }
            for (var index_perc = 0; index_perc < _this.progressLista.length; ++index_perc) {
                var percetagem_empresa = _this.checkProgress(index_perc, _this.progressLista[index_perc].empresa);
            }
        });
        console.log(this.progressLista);
    };
    HomePage.prototype.checkProgress = function (index, percetagem_empresa) {
        //BARRA DE PROGRESSO
        var _this = this;
        if ((localStorage.getItem('percentagem_' + percetagem_empresa + '') != undefined) && (localStorage.getItem('percentagem_' + percetagem_empresa + '') != '100') && (localStorage.getItem('percentagem_' + percetagem_empresa + '') != '0') && (localStorage.getItem('percentagem_' + percetagem_empresa + '') != null)) {
            this.percentagem = Observable.interval(1000).subscribe(function (x) {
                var valor = localStorage.getItem('percentagem_' + percetagem_empresa + '').split('.');
                _this.progress = valor[0];
                _this.progressLista[index].valor = _this.progress;
                if (_this.progress == '100') {
                    _this.percentagem.unsubscribe();
                }
            });
        }
        else {
            this.progressLista[index].valor = localStorage.getItem('percentagem_' + percetagem_empresa + '');
        }
    };
    //ATUALIZA A LISTA DE CATALOGOS DO USUARIO LOGADO
    HomePage.prototype.getUserCatalogoAtulizar = function (user) {
        var _this = this;
        this.http.get(this.ServidorProvider.urlGet() + 'usuarios_catalogos.php?user=' + user + '').pipe(map(function (res) { return res.json(); })).subscribe(function (dados) {
            if (dados.msg.carregado == 'sim') {
                _this.getUserCatList();
            }
        });
    };
    //PEGA A LISTA DE CATALOGOS DO USUARIO LOGADO
    HomePage.prototype.getUserCatList = function () {
        var _this = this;
        this.HomeProvider.goUserCatalogoList().then(function (data) {
            _this.userlistcat = data;
            _this.userlistcattoal = _this.userlistcat.length;
        });
    };
    HomePage.prototype.SaveCatalogo = function () {
        this.saveCatalogo = true;
    };
    //NEVEGAÇÃO FOOTER
    HomePage.prototype.goFooterLHomePage = function () { this.navCtrl.setRoot(HomePage_1); };
    ;
    HomePage.prototype.goFooterFavoritosPage = function () { this.navCtrl.push(FavoritosPage); };
    ;
    HomePage.prototype.goFooterPerfilPage = function () { this.navCtrl.push(PerfilPage); };
    ;
    //NEVEGA ATE A LISTA DA EMPRESA
    HomePage.prototype.goPageCategoria = function (categoria) {
        this.navCtrl.push(CategoriaPage, { categoria: categoria });
    };
    //NEVEGA ATE A LISTA DA EMPRESA
    HomePage.prototype.goLista = function (empresa_id, empresa_slug, empresa_nome, cor, total_produtos) {
        this.navCtrl.push(ListaPage, { empresa_id: empresa_id, empresa_slug: empresa_slug, empresa_nome: empresa_nome, cor: cor, total_produtos: total_produtos });
    };
    var HomePage_1;
    HomePage = HomePage_1 = __decorate([
        Component({
            selector: 'page-home',
            templateUrl: 'home.html',
            providers: [File]
        }),
        __metadata("design:paramtypes", [NavController, File, HomeProvider, ServidorProvider, Http, AlertController, LoadingController, Platform])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.js.map