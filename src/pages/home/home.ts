import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, Platform } from 'ionic-angular';

import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { AndroidPermissions } from '@ionic-native/android-permissions';

import { ListaPage } from '../lista/lista';

import { FavoritosPage } from '../favoritos/favoritos';
import { PerfilPage } from '../perfil/perfil';

import { CategoriaPage } from '../../pages/categoria/categoria';

import { HomeProvider } from '../../providers/home/home';
import { ServidorProvider } from '../../providers/servidor/servidor';
import { NetworkProvider } from '../../providers/network/network';


import { ScrollHideConfig } from '../../directives/scroll-hide/scroll-hide';
import { Observable } from 'rxjs/Rx';



declare var navigator: any;
declare var Connection: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [File, AndroidPermissions, FileTransfer]
})

export class HomePage {

  isConected: boolean;

  itens: any;
  categorias: any;
  categoriasLista: any = [];
  progressLista: any = [];
  cor: any;
  image_capa: any;
  lista_completa: any;
  listaslide: any[] = [];
  userlistcat: any;
  userlistcattoal: any;
  loadpage: any;
  saveCatalogo: boolean;
  slidesCatalogo: number = 2.3;
  percentagem: any;
  progress: any = 0;
  progress_valor: any = 0;

  alerta: any;
  public anArray: any = [];


  headerScrollConfig: ScrollHideConfig = { cssProperty: 'margin-top', maxValue: 56 };
  footerScrollConfig: ScrollHideConfig = { cssProperty: 'margin-bottom', maxValue: 56 };

  constructor(
    public networkprovider: NetworkProvider,
    private androidPermissions: AndroidPermissions,
    public navCtrl: NavController,
    public HomeProvider: HomeProvider,
    public ServidorProvider: ServidorProvider,
    public http: Http,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public platform: Platform
  ) { }

  ionViewWillEnter() {
    // Buscando se há conexão com a internet
    this.networkprovider.getIsOnline().subscribe((conection: boolean) => this.isConected = conection);
    console.log('CONECTION :: ', this.isConected);
  }

  ionViewDidLoad() {

    this.loadpage = false;

    if (localStorage.getItem('start') == 'OK') {

      //ELE PEGA A LISTA JA BAIXADA
      this.HomeProvider.getHome().then((data) => {
        this.itens = data;

        this.goCategorias();

        //PEGA A LISTA DE CATALOGOS DO USUARIO LOGADO
        this.getUserCatList();

        this.loadpage = true;
      });


    } else {

      // INCIA O APP PELA PRIMEIRA VEZ ELE BAIXA AS EMPRESAS
      this.HomeProvider.getStart().then((data) => {
        this.HomeProvider.getHome().then((data) => {

          //ATUALIZA A LISTA DE CATALOGOS DO USUARIO LOGADO
          this.getUserCatalogoAtulizar(localStorage.getItem('usuario_id'));

          this.itens = data;
          this.goCategorias();

          this.loadpage = true;

        });
      });

    }

    if (this.platform.width() > 1200) {
      this.slidesCatalogo = 6.3;
    } else if (this.platform.width() > 768) {
      this.slidesCatalogo = 5.3;
    } else if (this.platform.width() > 400) {
      this.slidesCatalogo = 3.3;
    } else if (this.platform.width() > 319) {
      this.slidesCatalogo = 2.3;
    }

  }


  //PEGA A LISTA DE CATEGORIA
  goCategorias() {
    this.HomeProvider.goCategorias().then((data) => {
      this.categorias = data;

      for (var i = 0; i < this.categorias.length; ++i) {

        for (var x = 0; x < this.categorias[i].empresas.length; ++x) {

          if (localStorage.getItem('percentagem_' + this.categorias[i].empresas[x].slug + '') != undefined) {
            this.progress_valor = localStorage.getItem('percentagem_' + this.categorias[i].empresas[x].slug + '');
          } else {
            this.progress_valor = '0';
          }

          this.progressLista.push({
            "empresa": this.categorias[i].empresas[x].slug,
            "valor": this.progress_valor
          });

        }

      }

      for (var z = 0; z < this.progressLista.length; ++z) {
        let percetagem_empresa = this.checkProgress(z, this.progressLista[z].empresa);
      }

    });

  }

  checkProgress(index, percetagem_empresa) {
    //BARRA DE PROGRESSO

    if ((localStorage.getItem('percentagem_' + percetagem_empresa + '') != undefined) && (localStorage.getItem('percentagem_' + percetagem_empresa + '') != '100')) {

      this.percentagem = Observable.interval(1000).subscribe(x => {

        var valor = localStorage.getItem('percentagem_' + percetagem_empresa + '').split('.');
        this.progress = valor[0];

        this.progressLista[index].valor = this.progress;

        if (this.progress == '100') {
          this.percentagem.unsubscribe();
        }


      });

    }
  }

  getSaveCatelogo(catalogo) {
    this.getUserCatalogoAtulizar(localStorage.getItem('usuario_id'));
  }

  //ATUALIZA A LISTA DE CATALOGOS DO USUARIO LOGADO
  getUserCatalogoAtulizar(user) {

    this.http.get(this.ServidorProvider.urlGet() + 'usuarios_catalogos.php?user=' + user + '').pipe(map(res => res.json())).subscribe(dados => {
      if (dados.msg.carregado == 'sim') {
        this.getUserCatList();
      }
    });

  }

  //PEGA A LISTA DE CATALOGOS DO USUARIO LOGADO
  getUserCatList() {

    this.HomeProvider.goUserCatalogoList().then((data) => {
      this.userlistcat = data;
      this.userlistcattoal = this.userlistcat.length;
    });

  }


  SaveCatalogo() {
    this.saveCatalogo = true;
  }

  //NEVEGAÇÃO FOOTER
  goFooterLHomePage() { this.navCtrl.setRoot(HomePage); };
  goFooterFavoritosPage() { this.navCtrl.push(FavoritosPage); };
  goFooterPerfilPage() { this.navCtrl.push(PerfilPage); };

  //NEVEGA ATE A LISTA DA EMPRESA
  goPageCategoria(categoria) {
    this.navCtrl.push(CategoriaPage, { categoria: categoria });
  }


  //NEVEGA ATE A LISTA DA EMPRESA
  goLista(empresa_id, empresa_slug, empresa_nome, cor, total_produtos) {
    this.navCtrl.push(ListaPage, { empresa_id: empresa_id, empresa_slug: empresa_slug, empresa_nome: empresa_nome, cor: cor, total_produtos: total_produtos });
  }


}