import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaPage } from './lista';

@NgModule({
  declarations: [
    ListaPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaPage),
  ],
  providers: [
    File,
    FileTransfer,
    FileTransferObject
  ]
})
export class ListaPageModule {}
