import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

import { ScrollHideConfig } from '../../directives/scroll-hide/scroll-hide';

import { ServidorProvider } from '../../providers/servidor/servidor';
import { ListaProvider } from '../../providers/lista/lista';
import { FavoritosProvider } from '../../providers/favoritos/favoritos';
import { HomeProvider } from '../../providers/home/home';
import { NetworkProvider } from '../../providers/network/network';

import { NotificationPage } from '../notification/notification';
import { DetalhePage } from '../detalhe/detalhe';
import { HomePage } from '../home/home';
import { FavoritosPage } from '../favoritos/favoritos';
import { PerfilPage } from '../perfil/perfil';

import { Keyboard } from '@ionic-native/keyboard';
import { Observable } from 'rxjs/Rx';
import { DomSanitizer } from '@angular/platform-browser';

declare let resolveLocalFileSystemURL;

@IonicPage()
@Component({
	selector: 'page-lista',
	templateUrl: 'lista.html',
})
export class ListaPage {

	headerScrollConfig: ScrollHideConfig = { cssProperty: 'margin-top', maxValue: 36 };
	footerScrollConfig: ScrollHideConfig = { cssProperty: 'margin-bottom', maxValue: 50 };

	isConected: boolean;
	msgoff: string;

	itens: any;
	empresa_dados: any;
	notification_total: any;
	filtros: any;
	aplicacoes: any;
	empresa_id: any;
	empresa_nome: any;
	empresa_slug: any;
	cor: any;
	total_produtos: any;
	percentagem: any;
	progress: any = 0;
	hideMe: any;
	hideMenu: any;
	loadpage: any;
	listaFavoritos: any;
	buttonLoad: string = '';

	users: any = [];
	staff: any = [];
	searchQuery: string = 'all';

	skip: any = 0;
	limit: any = 40;
	pesquisa: any;
	termo_item: any;

	selected: string = 'all';

	categorias: any;
	categoriasLista: any = [];
	progressLista: any = [];
	progress_valor: any = 0;


	userlista: any;
	userlistattoal: any;

	constructor(
		public networkprovider: NetworkProvider,
		public ServidorProvider: ServidorProvider,
		public HomeProvider: HomeProvider,
		public ListaProvider: ListaProvider,
		public FavoritosProvider: FavoritosProvider,
		public loadingCtrl: LoadingController,
		public alertCtrl: AlertController,
		public navCtrl: NavController,
		public navParams: NavParams,
		public keyboard: Keyboard,
		public http: Http,
		private file: File,
		private fileTransfer: FileTransfer,
		private domSanitizer: DomSanitizer
	) {

		this.empresa_id = this.navParams.get('empresa_id');
		this.empresa_slug = this.navParams.get('empresa_slug');
		this.empresa_nome = this.navParams.get('empresa_nome');
		this.cor = this.navParams.get('cor');
		this.total_produtos = this.navParams.get('total_produtos');
		this.listaFavoritos = this.navParams.get('lista');

		// Buscando se há conexão com a internet
		this.networkprovider.getIsOnline().subscribe((connection: boolean) => this.isConected = connection);

		//ELE PEGA A LISTA JA BAIXADA
		this.HomeProvider.getEmpresaLista(this.empresa_id).then((data) => {
			this.empresa_dados = data;
			this.filtros = this.empresa_dados[0].filtros;
			this.notification_total = 0;
			for (var i = 0; i < this.empresa_dados[0].notificacoes.length; ++i) {
				if (this.empresa_dados[0].notificacoes[i].lido == 'n_lido') {
					this.notification_total = (this.notification_total + 1);
				}
			}
		});


		this.loadpage = false;

		if (localStorage.getItem('empresa_' + this.empresa_slug + '') == 'OK') {

			//this.getPermission();

			//ELE PEGA A LISTA JA BAIXADA
			this.hideMe = true;

			this.ListaProvider.getLista(this.empresa_slug, this.empresa_id, this.total_produtos, this.listaFavoritos, '', this.skip, this.limit).then(data => {
				this.loadpage = true;
				this.staff = data;
				this.initializeItems(this.staff);
			});


		} else {

			if (this.isConected) {

				// INCIA O APP PELA PRIMEIRA VEZ ELE BAIXA AS EMPRESAS
				//this.disable(1);
				this.hideMe = false;
				this.loadpage = false;
				this.msgoff = '';
				localStorage.setItem('percentagem', '0');
				this.ListaProvider.getProdutos(this.empresa_slug, this.empresa_id, this.total_produtos, this.listaFavoritos).then((data) => {
					this.loadpage = true;

					this.ListaProvider.getLista(this.empresa_slug, this.empresa_id, this.total_produtos, this.listaFavoritos, '', this.skip, this.limit).then(data => {
						this.staff = data;
						this.initializeItems(this.staff, true);
						localStorage.setItem('empresa_' + this.empresa_slug + '', 'OK');
					});

				});

				console.log('ENTROU');

			} else {

				this.hideMe = true;
				this.loadpage = true;

				if (localStorage.getItem('empresa_' + this.empresa_slug + '') == undefined) {
					this.hideMenu = true;
					this.msgoff = 'VOCÊ PRECISA DE CONEXÃO COM A INTERNET PARA BAIXAR ESTE CATÁLOGO PELA PRIMEIRA VEZ.';
				}

			}


		}

		if (this.isConected) {
			//BARRA DE PROGRESSO
			this.percentagem = Observable.interval(1000).subscribe(x => {
				var valor = localStorage.getItem('percentagem').split('.');
				this.progress = valor[0];

				if (valor[0] == '100') {
					this.ListaProvider.getLista(this.empresa_slug, this.empresa_id, this.total_produtos, this.listaFavoritos, '', this.skip, this.limit).then((data: any[]) => {
						this.staff = data;
						this.initializeItems(this.staff);
						this.hideMe = true;
						this.loadpage = true;
					});

					this.percentagem.unsubscribe();
				}
			});

		} else {
			this.hideMe = true;
			this.loadpage = true;
		}

	}

	downloadImg(item) {
		item.downloading = true;
		console.log("CAPA PARA BAIXAR ::", item.capa);
		const [originPath, pathProduto, ano, mes, name] = item.capa.split('/');
		const directory = `${this.file.cacheDirectory}${pathProduto}`;
		this.checkDirectory(pathProduto);

		const transfer: FileTransferObject = this.fileTransfer.create();
		const url = encodeURI(`${this.ServidorProvider.getOriginPathImgs()}${item.capa}`);
		console.log("URL PARA DOWNLOAD :: ", url);

		transfer.download(url, `${directory}/${name}`, true).then((res) => {
			console.log("IMG DOWNLOADED ? :: ", res);
			this.file.readAsDataURL(`${directory}`, name).then((data) => {
				item.newCapa = this.domSanitizer.bypassSecurityTrustUrl(data);
				item.downloading = false;
				item.downloaded = true;
			});
		}).catch((e) => {
			item.downloading = false;
			console.error("FALHA AO TENTAR FAZER O DOWNLOAD DA IMAGEM :: ", e)
		});
	}

	private checkDirectory(pathProduto) {
		this.file.checkDir(this.file.cacheDirectory, pathProduto).then((res) => {
			console.log("PASTA EXISTE ? ", res);
		}).catch((e) => {
			console.log("PASTA EXISTE ERRO ? ", e);
			this.file.createDir(this.file.cacheDirectory, pathProduto, false).then((res) => {
				console.log("PASTA CRIADA :: ", res);
			}).catch((e) => {
				console.log("ERRO AO CRIAR PASTA :: ", e);
			})
		})
	}

	private verifyCapaDownloaded(path: string, capa: string): Promise<boolean> {
		return new Promise((resolve, reject) => {
			this.file.checkFile(this.file.cacheDirectory, `${path}/${capa}`).then(() => {
				resolve(true);
			}).catch(() => {
				console.log('FILE NÃO EXIST :: ', capa);
				reject(false);
			});
		});
	}

	initializeItems(staff, isFirstAccess?: boolean) {
		staff.map((item) => {
			item.downloading = true;
			this.chekItens(item, isFirstAccess);
		});
		this.users = staff;
	}

	chekItens(item, isFirstAccess?: boolean): Promise<any> {
		return new Promise((resolve, reject) => {
			const [originPath, pathProduto, ano, mes, name] = item.capa.split('/');
			this.verifyCapaDownloaded(pathProduto, name).then((res) => {
				this.file.readAsDataURL(`${this.file.cacheDirectory}${pathProduto}`, name).then((data) => {
					item.newCapa = this.domSanitizer.bypassSecurityTrustUrl(data);
					item.downloaded = res;
					item.downloading = false;
					resolve(item);
				});
			}).catch(() => {
				if (isFirstAccess) {
					this.downloadImg(item);
				} else {
					item.downloaded = false;
					item.downloading = false;
				}
				resolve(item);
			});
		})
	}


	//PEGA A LISTA DE CATEGORIA
	goCategorias() {

		for (var z = 0; z < this.progressLista.length; ++z) {
			let percetagem_empresa = this.checkProgress(z, this.progressLista[z].empresa);
		}
		console.log(this.progressLista);
	}

	//BARRA DE PROGRESSO
	checkProgress(index, percetagem_empresa) {

		if ((localStorage.getItem('percentagem_' + percetagem_empresa + '') != undefined) && (localStorage.getItem('percentagem_' + percetagem_empresa + '') != '100') && (localStorage.getItem('percentagem_' + percetagem_empresa + '') != '0') && (localStorage.getItem('percentagem_' + percetagem_empresa + '') != null)) {

			this.percentagem = Observable.interval(1000).subscribe(x => {

				var valor = localStorage.getItem('percentagem_' + percetagem_empresa + '').split('.');
				this.progress = valor[0];

				this.progressLista[index].valor = this.progress;

				if (this.progress == '100') {
					this.percentagem.unsubscribe();
				}

			});

		}


	}


	getItems(ev) {

		this.initializeItems(this.staff);

		var val = ev.target.value;

		if (val && val.trim() != '') {
			this.users = this.users.filter((user) => {

				return (user.Description.toLowerCase().indexOf(val.toLowerCase()) > -1);
			})
		}
	}

	doInfinite(infiniteScroll) {
		setTimeout(() => {
			this.skip = this.skip + this.limit;
			if (this.skip >= this.total_produtos) {
				infiniteScroll.enable(false);
			} else {
				if (this.termo_item == undefined) {
					this.ListaProvider.getLista(this.empresa_slug, this.empresa_id, this.total_produtos, this.listaFavoritos, '', this.skip, this.limit).then((data) => {
						for (var i = 0; i < this.limit; i++) {
							this.chekItens(data[i], true).then((item) => {
								console.log(data[i], item);
								this.users.push(item);
							});
						}
					});
				} else {
					this.ListaProvider.goSearchLista(this.empresa_slug, this.empresa_id, this.total_produtos, this.listaFavoritos, this.termo_item, this.skip, this.limit).then((data) => {
						for (var i = 0; i < this.limit; i++) {
							if (data[i] != undefined) {
								// this.users.push(data[i]);
								this.chekItens(data[i], true).then((item) => {
									console.log(data[i], item);
									this.users.push(item);
								});
							} else {
								infiniteScroll.enable(false);
							}
						}
					});
				}
			}
			infiniteScroll.complete();
		}, 250);
	}

	goSearchLista(termo_item: any, selected) {

		this.pesquisa = '';
		this.termo_item = termo_item;

		if (selected != '') {
			this.selected = selected;
		} else {
			this.selected = 'all';
		}

		if ((selected != 'all') && (termo_item == undefined)) {

			this.pesquisa = this.selected;

		} else if ((selected == 'all') && (termo_item != undefined)) {

			this.pesquisa = termo_item;

		} else {

			this.pesquisa = termo_item + ' ' + this.selected;

		}

		this.loadpage = false;
		this.keyboard.hide();

		this.ListaProvider.goSearchLista(this.empresa_slug, this.empresa_id, this.total_produtos, this.listaFavoritos, this.pesquisa, this.skip, this.limit).then((data) => {
			this.users = data;
			this.loadpage = true;
		});

	}


	goDetalhe(produto: any) {
		this.navCtrl.push(DetalhePage, { produto: produto, empresa_slug: this.empresa_slug, empresa_nome: this.empresa_nome, cor: this.cor });
	}

	goNotification() {
		this.navCtrl.push(NotificationPage, { empresa_id: this.empresa_id, empresa_slug: this.empresa_slug, empresa_nome: this.empresa_nome, cor: this.cor });
	}

	sincInfo() {
		let alert = this.alertCtrl.create({
			title: 'Porque sincronizar?',
			subTitle: 'Estamos verificando atualizações e novos produtos. Isso só será feito uma vez não se preocupe.',
			buttons: ['ENTENDI']
		});
		alert.present();
	}


	async selectLista(item, empresa) {

		this.FavoritosProvider.goUserListasList(empresa).then((data) => {

			this.userlista = data;
			this.userlistattoal = this.userlista.listas;


			let alert = this.alertCtrl.create();
			alert.setTitle('Salvar em...');

			for (let z = 0; z < this.userlista.length; z++) {

				for (let i = 0; i < this.userlista[z].listas.length; i++) {

					if (this.userlista[z]._id == empresa) {

						var check_item = false;
						var produtos = this.userlista[z].listas[i].produtos;

						if (produtos != false) {

							var check = produtos.split(',');

							for (var x = 0; x < check.length; ++x) {
								if (check[x] == item) {
									var check_item = true;
								}
							}

						};

						var valor = item + '-' + this.userlista[z].listas[i].id_lista + '-' + this.userlista[z].listas[i].id_user + '-' + this.userlista[z].listas[i].id_empresa;

						alert.addInput({
							type: 'checkbox',
							label: this.userlista[z].listas[i].nome,
							value: valor,
							checked: check_item
						});

					};
				};
			};


			alert.addButton({ text: 'CANCELAR', role: 'cancel', cssClass: 'danger' });
			alert.addButton({
				text: 'SALVAR',
				handler: data => {
					this.saveList(data);
				}
			});
			alert.present();

		});

	}

	saveList(dados) {

		this.buttonLoad = 'buttonLoad';

		this.http.get(this.ServidorProvider.urlGet() + 'usuarios_listas_save.php?dados=' + dados + '').pipe(map(res => res.json())).subscribe(dados => {
			if (dados.msg.carregado == 'sim') {
				this.FavoritosProvider.clearUserListasList().then((data) => {
					localStorage.setItem('FavoritosPendente', 'YES');
					this.getUserListasAtulizar(localStorage.getItem('usuario_id'));
				});


				//this.getUserCatList();
			}
		});

		console.log(dados);
	}

	//ATUALIZA A LISTA DE LISTAS DO USUARIO LOGADO
	getUserListasAtulizar(user) {

		this.http.get(this.ServidorProvider.urlGet() + 'usuarios_listas.php?user=' + user + '').pipe(map(res => res.json())).subscribe(dados => {
			if (dados.msg.carregado == 'sim') {
				this.startUserListasList();
			}
		});

	}

	startUserListasList() {

		this.FavoritosProvider.startUserListasList().then((data) => {
			this.getUserListasList();
		});

	}

	getUserListasList() {

		this.FavoritosProvider.goUserListasList('').then((data) => {
			this.userlista = data;
			this.userlistattoal = this.userlista.length;
			localStorage.setItem('FavoritosPendente', 'NO');
			this.buttonLoad = '';
		});

	}


	voltar() {
		if (this.listaFavoritos != undefined) {
			this.navCtrl.pop();
		} else {
			this.navCtrl.setRoot(HomePage);
		}
	}

	//NEVEGAÇÃO FOOTER
	goFooterLHomePage() { this.navCtrl.setRoot(HomePage); };
	goFooterFavoritosPage() { this.navCtrl.push(FavoritosPage); };
	goFooterPerfilPage() { this.navCtrl.push(PerfilPage); };


}

// 	// disable(status) {
// 	// 	if(status == 1){
// 	// 		const inputs: any = document.getElementById("buscar").getElementsByTagName("INPUT");
// 	// 		inputs[0].disabled=true;
// 	// 	}else{
// 	// 		const inputs: any = document.getElementById("buscar").getElementsByTagName("INPUT");
// 	// 		inputs[0].disabled=false;
// 	// 	}
//  //    }
