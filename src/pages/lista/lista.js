var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';
import { ServidorProvider } from '../../providers/servidor/servidor';
import { ListaProvider } from '../../providers/lista/lista';
import { FavoritosProvider } from '../../providers/favoritos/favoritos';
import { HomeProvider } from '../../providers/home/home';
import { NotificationPage } from '../notification/notification';
import { DetalhePage } from '../detalhe/detalhe';
import { HomePage } from '../home/home';
import { FavoritosPage } from '../favoritos/favoritos';
import { PerfilPage } from '../perfil/perfil';
import { Keyboard } from '@ionic-native/keyboard';
import { Observable } from 'rxjs/Rx';
var ListaPage = /** @class */ (function () {
    function ListaPage(ServidorProvider, HomeProvider, ListaProvider, FavoritosProvider, loadingCtrl, alertCtrl, navCtrl, navParams, keyboard, http) {
        var _this = this;
        this.ServidorProvider = ServidorProvider;
        this.HomeProvider = HomeProvider;
        this.ListaProvider = ListaProvider;
        this.FavoritosProvider = FavoritosProvider;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.keyboard = keyboard;
        this.http = http;
        this.headerScrollConfig = { cssProperty: 'margin-top', maxValue: 36 };
        this.footerScrollConfig = { cssProperty: 'margin-bottom', maxValue: 50 };
        this.progress = 0;
        this.buttonLoad = '';
        this.users = [];
        this.staff = [];
        this.searchQuery = 'all';
        this.skip = 0;
        this.limit = 40;
        this.selected = 'all';
        this.categoriasLista = [];
        this.progressLista = [];
        this.progress_valor = 0;
        this.empresa_id = this.navParams.get('empresa_id');
        this.empresa_slug = this.navParams.get('empresa_slug');
        this.empresa_nome = this.navParams.get('empresa_nome');
        this.cor = this.navParams.get('cor');
        this.total_produtos = this.navParams.get('total_produtos');
        this.listaFavoritos = this.navParams.get('lista');
        //ELE PEGA A LISTA JA BAIXADA
        this.HomeProvider.getEmpresaLista(this.empresa_id).then(function (data) {
            _this.empresa_dados = data;
            _this.filtros = _this.empresa_dados[0].filtros;
            _this.notification_total = 0;
            for (var i = 0; i < _this.empresa_dados[0].notificacoes.length; ++i) {
                if (_this.empresa_dados[0].notificacoes[i].lido == 'n_lido') {
                    _this.notification_total = (_this.notification_total + 1);
                }
            }
        });
        this.loadpage = false;
        if (localStorage.getItem('empresa_' + this.empresa_slug + '') == 'OK') {
            //ELE PEGA A LISTA JA BAIXADA
            this.hideMe = true;
            this.ListaProvider.getLista(this.empresa_slug, this.empresa_id, this.total_produtos, this.listaFavoritos, '', this.skip, this.limit).then(function (data) {
                _this.loadpage = true;
                _this.staff = data;
                _this.initializeItems();
            });
        }
        else {
            // INCIA O APP PELA PRIMEIRA VEZ ELE BAIXA AS EMPRESAS
            //this.disable(1);
            this.hideMe = false;
            localStorage.setItem('percentagem', '0');
            this.ListaProvider.getProdutos(this.empresa_slug, this.empresa_id, this.total_produtos, this.listaFavoritos).then(function (data) {
                _this.loadpage = true;
                _this.ListaProvider.getLista(_this.empresa_slug, _this.empresa_id, _this.total_produtos, _this.listaFavoritos, '', _this.skip, _this.limit).then(function (data) {
                    _this.staff = data;
                    _this.initializeItems();
                    localStorage.setItem('empresa_' + _this.empresa_slug + '', 'OK');
                });
            });
        }
        //BARRA DE PROGRESSO
        this.percentagem = Observable.interval(1000).subscribe(function (x) {
            var valor = localStorage.getItem('percentagem').split('.');
            _this.progress = valor[0];
            if (valor[0] == '100') {
                _this.ListaProvider.getLista(_this.empresa_slug, _this.empresa_id, _this.total_produtos, _this.listaFavoritos, '', _this.skip, _this.limit).then(function (data) {
                    _this.staff = data;
                    _this.initializeItems();
                    _this.hideMe = true;
                    _this.loadpage = true;
                });
                _this.percentagem.unsubscribe();
            }
        });
        //get data from user provider
        // this.ListaProvider.getUsers(this.empresa_slug, this.empresa_id, this.total_produtos, this.listaFavoritos, this.skip, this.limit).then(data => {
        // 	this.staff = data;
        // 	this.initializeItems();
        // });
    }
    ListaPage.prototype.initializeItems = function () {
        this.goCategorias();
        this.users = this.staff;
    };
    //PEGA A LISTA DE CATEGORIA
    ListaPage.prototype.goCategorias = function () {
        var _this = this;
        this.HomeProvider.goCategorias().then(function (data) {
            _this.categorias = data;
            for (var cat = 0; cat < _this.categorias.length; ++cat) {
                for (var emp = 0; emp < _this.categorias[cat].empresas.length; ++emp) {
                    if ((localStorage.getItem('percentagem_' + _this.categorias[cat].empresas[emp].slug + '') != undefined) || (localStorage.getItem('percentagem_' + _this.categorias[cat].empresas[emp].slug + '') != null)) {
                        _this.progress_valor = localStorage.getItem('percentagem_' + _this.categorias[cat].empresas[emp].slug + '');
                    }
                    else {
                        _this.progress_valor = '0';
                    }
                    _this.progressLista.push({
                        "empresa": _this.categorias[cat].empresas[emp].slug,
                        "valor": _this.progress_valor
                    });
                }
            }
            for (var index_perc = 0; index_perc < _this.progressLista.length; ++index_perc) {
                var percetagem_empresa = _this.checkProgress(index_perc, _this.progressLista[index_perc].empresa);
            }
        });
        console.log(this.progressLista);
    };
    //BARRA DE PROGRESSO
    ListaPage.prototype.checkProgress = function (index, percetagem_empresa) {
        var _this = this;
        if ((localStorage.getItem('percentagem_' + percetagem_empresa + '') != undefined) && (localStorage.getItem('percentagem_' + percetagem_empresa + '') != '100') && (localStorage.getItem('percentagem_' + percetagem_empresa + '') != '0') && (localStorage.getItem('percentagem_' + percetagem_empresa + '') != null)) {
            this.percentagem = Observable.interval(1000).subscribe(function (x) {
                var valor = localStorage.getItem('percentagem_' + percetagem_empresa + '').split('.');
                _this.progress = valor[0];
                _this.progressLista[index].valor = _this.progress;
                console.log(_this.progress);
                if (_this.progress == '100') {
                    _this.percentagem.unsubscribe();
                }
            });
        }
    };
    ListaPage.prototype.getItems = function (ev) {
        this.initializeItems();
        var val = ev.target.value;
        if (val && val.trim() != '') {
            this.users = this.users.filter(function (user) {
                return (user.Description.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    ListaPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        setTimeout(function () {
            _this.skip = _this.skip + _this.limit;
            if (_this.skip >= _this.total_produtos) {
                infiniteScroll.enable(false);
            }
            else {
                if (_this.termo_item == undefined) {
                    _this.ListaProvider.getLista(_this.empresa_slug, _this.empresa_id, _this.total_produtos, _this.listaFavoritos, '', _this.skip, _this.limit).then(function (data) {
                        for (var i = 0; i < _this.limit; i++) {
                            _this.users.push(data[i]);
                        }
                    });
                }
                else {
                    _this.ListaProvider.goSearchLista(_this.empresa_slug, _this.empresa_id, _this.total_produtos, _this.listaFavoritos, _this.termo_item, _this.skip, _this.limit).then(function (data) {
                        for (var i = 0; i < _this.limit; i++) {
                            if (data[i] != undefined) {
                                _this.users.push(data[i]);
                            }
                            else {
                                infiniteScroll.enable(false);
                            }
                        }
                    });
                }
            }
            infiniteScroll.complete();
        }, 250);
    };
    ListaPage.prototype.goSearchLista = function (termo_item, selected) {
        var _this = this;
        this.pesquisa = '';
        this.termo_item = termo_item;
        if (selected != '') {
            this.selected = selected;
        }
        else {
            this.selected = 'all';
        }
        if ((selected != 'all') && (termo_item == undefined)) {
            this.pesquisa = this.selected;
        }
        else if ((selected == 'all') && (termo_item != undefined)) {
            this.pesquisa = termo_item;
        }
        else {
            this.pesquisa = termo_item + ' ' + this.selected;
        }
        this.loadpage = false;
        this.keyboard.hide();
        this.ListaProvider.goSearchLista(this.empresa_slug, this.empresa_id, this.total_produtos, this.listaFavoritos, this.pesquisa, this.skip, this.limit).then(function (data) {
            _this.users = data;
            _this.loadpage = true;
        });
    };
    ListaPage.prototype.goDetalhe = function (produto) {
        this.navCtrl.push(DetalhePage, { produto: produto, empresa_slug: this.empresa_slug, empresa_nome: this.empresa_nome, cor: this.cor });
    };
    ListaPage.prototype.goNotification = function () {
        this.navCtrl.push(NotificationPage, { empresa_id: this.empresa_id, empresa_slug: this.empresa_slug, empresa_nome: this.empresa_nome, cor: this.cor });
    };
    ListaPage.prototype.sincInfo = function () {
        var alert = this.alertCtrl.create({
            title: 'Porque sincronizar?',
            subTitle: 'Estamos verificando atualizações e novos produtos. Isso só será feito uma vez não se preocupe.',
            buttons: ['ENTENDI']
        });
        alert.present();
    };
    ListaPage.prototype.selectLista = function (item, empresa) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.FavoritosProvider.goUserListasList(empresa).then(function (data) {
                    _this.userlista = data;
                    _this.userlistattoal = _this.userlista.listas;
                    var alert = _this.alertCtrl.create();
                    alert.setTitle('Salvar em...');
                    for (var z = 0; z < _this.userlista.length; z++) {
                        for (var i = 0; i < _this.userlista[z].listas.length; i++) {
                            if (_this.userlista[z]._id == empresa) {
                                var check_item = false;
                                var produtos = _this.userlista[z].listas[i].produtos;
                                if (produtos != false) {
                                    var check = produtos.split(',');
                                    for (var x = 0; x < check.length; ++x) {
                                        if (check[x] == item) {
                                            var check_item = true;
                                        }
                                    }
                                }
                                ;
                                var valor = item + '-' + _this.userlista[z].listas[i].id_lista + '-' + _this.userlista[z].listas[i].id_user + '-' + _this.userlista[z].listas[i].id_empresa;
                                alert.addInput({
                                    type: 'checkbox',
                                    label: _this.userlista[z].listas[i].nome,
                                    value: valor,
                                    checked: check_item
                                });
                            }
                            ;
                        }
                        ;
                    }
                    ;
                    alert.addButton({ text: 'CANCELAR', role: 'cancel', cssClass: 'danger' });
                    alert.addButton({ text: 'SALVAR',
                        handler: function (data) {
                            _this.saveList(data);
                        }
                    });
                    alert.present();
                });
                return [2 /*return*/];
            });
        });
    };
    ListaPage.prototype.saveList = function (dados) {
        var _this = this;
        this.buttonLoad = 'buttonLoad';
        this.http.get(this.ServidorProvider.urlGet() + 'usuarios_listas_save.php?dados=' + dados + '').pipe(map(function (res) { return res.json(); })).subscribe(function (dados) {
            if (dados.msg.carregado == 'sim') {
                _this.FavoritosProvider.clearUserListasList().then(function (data) {
                    localStorage.setItem('FavoritosPendente', 'YES');
                    _this.getUserListasAtulizar(localStorage.getItem('usuario_id'));
                });
                //this.getUserCatList();
            }
        });
        console.log(dados);
    };
    //ATUALIZA A LISTA DE LISTAS DO USUARIO LOGADO
    ListaPage.prototype.getUserListasAtulizar = function (user) {
        var _this = this;
        this.http.get(this.ServidorProvider.urlGet() + 'usuarios_listas.php?user=' + user + '').pipe(map(function (res) { return res.json(); })).subscribe(function (dados) {
            if (dados.msg.carregado == 'sim') {
                _this.startUserListasList();
            }
        });
    };
    ListaPage.prototype.startUserListasList = function () {
        var _this = this;
        this.FavoritosProvider.startUserListasList().then(function (data) {
            _this.getUserListasList();
        });
    };
    ListaPage.prototype.getUserListasList = function () {
        var _this = this;
        this.FavoritosProvider.goUserListasList('').then(function (data) {
            _this.userlista = data;
            _this.userlistattoal = _this.userlista.length;
            localStorage.setItem('FavoritosPendente', 'NO');
            _this.buttonLoad = '';
        });
    };
    ListaPage.prototype.voltar = function () {
        this.navCtrl.pop();
    };
    //NEVEGAÇÃO FOOTER
    ListaPage.prototype.goFooterLHomePage = function () { this.navCtrl.setRoot(HomePage); };
    ;
    ListaPage.prototype.goFooterFavoritosPage = function () { this.navCtrl.push(FavoritosPage); };
    ;
    ListaPage.prototype.goFooterPerfilPage = function () { this.navCtrl.push(PerfilPage); };
    ;
    ListaPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-lista',
            templateUrl: 'lista.html',
        }),
        __metadata("design:paramtypes", [ServidorProvider, HomeProvider, ListaProvider, FavoritosProvider, LoadingController, AlertController, NavController, NavParams, Keyboard, Http])
    ], ListaPage);
    return ListaPage;
}());
export { ListaPage };
// 	// disable(status) {
// 	// 	if(status == 1){
// 	// 		const inputs: any = document.getElementById("buscar").getElementsByTagName("INPUT");
// 	// 		inputs[0].disabled=true;
// 	// 	}else{
// 	// 		const inputs: any = document.getElementById("buscar").getElementsByTagName("INPUT");
// 	// 		inputs[0].disabled=false;
// 	// 	}
//  //    }
//# sourceMappingURL=lista.js.map