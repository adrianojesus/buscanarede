var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { DetalheProvider } from '../../providers/detalhe/detalhe';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { HomePage } from '../home/home';
import { FavoritosPage } from '../favoritos/favoritos';
import { PerfilPage } from '../perfil/perfil';
var DetalhePage = /** @class */ (function () {
    function DetalhePage(navCtrl, navParams, DetalheProvider, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.DetalheProvider = DetalheProvider;
        this.alertCtrl = alertCtrl;
        this.headerScrollConfig = { cssProperty: 'margin-top', maxValue: 56 };
        this.footerScrollConfig = { cssProperty: 'margin-bottom', maxValue: 56 };
        this.produto = this.navParams.get('produto');
        this.empresa_slug = this.navParams.get('empresa_slug');
        this.empresa_nome = this.navParams.get('empresa_nome');
        this.cor = this.navParams.get('cor');
    }
    DetalhePage_1 = DetalhePage;
    DetalhePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.DetalheProvider.getProdutoDetalhe(this.produto, this.empresa_slug).then(function (data) {
            _this.itens = data;
        });
    };
    DetalhePage.prototype.next = function () {
        this.slides.slideNext();
    };
    DetalhePage.prototype.prev = function () {
        this.slides.slidePrev();
    };
    DetalhePage.prototype.ShowOriginal = function (dados_referencias) {
        this.lista_referencias = '';
        for (var r = 0; r < dados_referencias.length; r++) {
            if (dados_referencias[r].codigo != '') {
                this.lista_referencias += '<div>' + dados_referencias[r].codigo + '</div>';
            }
        }
        var alert = this.alertCtrl.create({
            title: 'CÓDIGO ORIGINAL',
            subTitle: this.lista_referencias,
            buttons: ['OK']
        });
        alert.present();
    };
    DetalhePage.prototype.ShowEquivalencias = function (dados_equivalencias) {
        this.lista_equivalencias = '';
        for (var e = 0; e < dados_equivalencias.length; e++) {
            if (dados_equivalencias[e].codigo != '') {
                this.lista_equivalencias += '<div>' + dados_equivalencias[e].codigo + '</div>';
            }
        }
        var alert = this.alertCtrl.create({
            title: 'EQUIVALÊNCIAS',
            subTitle: this.lista_equivalencias,
            buttons: ['OK']
        });
        alert.present();
    };
    DetalhePage.prototype.ShowRelacionados = function (item, montadora, modelo, anos) {
        var _this = this;
        console.log(anos);
        var dados_anos = anos.split('|');
        this.DetalheProvider.ShowRelacionados(this.empresa_slug, item, '', montadora, modelo, dados_anos[0], dados_anos[dados_anos.length - 1]).then(function (data) {
            _this.relacionados = data;
            console.log(_this.relacionados);
        });
    };
    DetalhePage.prototype.goDetalheSubitem = function (produto) {
        this.navCtrl.push(DetalhePage_1, { produto: produto, empresa_slug: this.empresa_slug, empresa_nome: this.empresa_nome, cor: this.cor });
    };
    //NEVEGAÇÃO FOOTER
    DetalhePage.prototype.goFooterLHomePage = function () { this.navCtrl.setRoot(HomePage); };
    ;
    DetalhePage.prototype.goFooterFavoritosPage = function () { this.navCtrl.push(FavoritosPage); };
    ;
    DetalhePage.prototype.goFooterPerfilPage = function () { this.navCtrl.push(PerfilPage); };
    ;
    DetalhePage.prototype.voltar = function () {
        this.navCtrl.pop();
    };
    var DetalhePage_1;
    __decorate([
        ViewChild(Slides),
        __metadata("design:type", Slides)
    ], DetalhePage.prototype, "slides", void 0);
    DetalhePage = DetalhePage_1 = __decorate([
        IonicPage(),
        Component({
            selector: 'page-detalhe',
            templateUrl: 'detalhe.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, DetalheProvider, AlertController])
    ], DetalhePage);
    return DetalhePage;
}());
export { DetalhePage };
//# sourceMappingURL=detalhe.js.map