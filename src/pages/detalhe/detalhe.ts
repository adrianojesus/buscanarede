import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ServidorProvider } from './../../providers/servidor/servidor';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Img } from 'ionic-angular';
import { DetalheProvider } from '../../providers/detalhe/detalhe';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';

import { HomePage } from '../home/home';
import { FavoritosPage } from '../favoritos/favoritos';
import { PerfilPage } from '../perfil/perfil';

import { ScrollHideConfig } from '../../directives/scroll-hide/scroll-hide';

@IonicPage()
@Component({
	selector: 'page-detalhe',
	templateUrl: 'detalhe.html',
})
export class DetalhePage  {

	@ViewChild(Slides) slides: Slides;

	itens: any;
	produto: any;
	empresa_slug: any;
	empresa_nome: any;
	cor: any;
	relacionados: any;
	lista_referencias: any;
	lista_equivalencias: any;

	headerScrollConfig: ScrollHideConfig = { cssProperty: 'margin-top', maxValue: 56 };
	footerScrollConfig: ScrollHideConfig = { cssProperty: 'margin-bottom', maxValue: 56 };

	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams, 
		public DetalheProvider: DetalheProvider, 
		public alertCtrl: AlertController,
		private file: File,
		private fileTransfer: FileTransfer,
		private servidorProvider: ServidorProvider,
		private domSanitizer: DomSanitizer
	) {
		this.produto = this.navParams.get('produto');
		this.empresa_slug = this.navParams.get('empresa_slug');
		this.empresa_nome = this.navParams.get('empresa_nome');
		this.cor = this.navParams.get('cor');
	}

	ionViewWillEnter(){
		this.DetalheProvider.getProdutoDetalhe(this.produto, this.empresa_slug).then((data: any[]) => {
			// data.map((item) =>  { return this.verifyImgs(item) });
			this.initializeItens(data);
		});
	}

	initializeItens(data: any[]): Promise<any>{
		let arrayPromises = [];
		for (let i = 0; i < data.length; i++) {
			arrayPromises.push( this.verifyItem(data[i]) );
		}
		return Promise.all(arrayPromises).then(()=>{
			console.log(data);
			this.itens = data;
		});
	}

	verifyItem(item): Promise<any> {
		return new Promise((resolve, reject) => {
			const pathProduto = item.capa.split('/')[1];
			const directory = `${this.file.cacheDirectory}${pathProduto}`;
			let arrayPromises = [];

			for (let i = 0; i < item.imagens.length; i++) {
				arrayPromises.push(this.verifyImg(item.imagens[i], pathProduto, directory));	
			}

			Promise.all(arrayPromises).then((dataimg: any[]) =>{
				for (let i = 0; i < item.imagens.length; i++) {
					item.imagens[i] = dataimg[i];				
				}
				resolve(item);
			});
		});
	}

	verifyImg(img, pathProduto, directory): Promise<any> {
		return new Promise((resolve, reject) => {
			const name = img.imagem.split('/')[img.imagem.split('/').length -1];
			this.verifyImgsDownloaded(`${pathProduto}`, name).then(()=> {
				this.file.readAsDataURL(`${directory}`, name).then((dataUrl) => {
					img.imagem = this.domSanitizer.bypassSecurityTrustUrl(dataUrl);
					resolve(img);
				});
			}).catch(()=> {
				this.downloadImg(img.imagem, pathProduto, directory).then((url) => {
					img.imagem = url;
					resolve(img);
				}).catch((e) => {
					console.log(e);
					img.imagem = "assets/imgs/download-img.svg";
					resolve(img);
				});
			});	
		});
	}



	private downloadImg(imagem, pathProduto, directory): Promise<any> {
		return new Promise((resolve, reject) => {
			console.log("IMAGENS PARA BAIXAR::", imagem);
			this.checkDirectory(pathProduto);
			const transfer: FileTransferObject = this.fileTransfer.create();
			const url = encodeURI(`${this.servidorProvider.getOriginPathImgs()}${imagem}`);
			const name = imagem.split('/')[imagem.split('/').length -1];
			console.log("URL PARA DOWNLOAD :: ", url);
	
			transfer.download(url, `${directory}${name}`, true).then((res) => {
				console.log("IMG DOWNLOADED ? :: ", res);
				this.file.readAsDataURL(`${directory}`, name).then((data) => {
					resolve(this.domSanitizer.bypassSecurityTrustUrl(data));
				});
			}).catch((e) => {
				console.error("FALHA AO TENTAR FAZER O DOWNLOAD DA IMAGEM :: ", e)
				reject(e);
			});
		});
	}

	private checkDirectory(pathProduto) {
		this.file.checkDir(this.file.cacheDirectory, `${pathProduto}/imgs`).then((res) => {
			console.log("PASTA EXISTE ? ", res);
		}).catch((e) => {
			console.log("PASTA EXISTE ERRO ? ", e);
			this.file.createDir(this.file.cacheDirectory, `${pathProduto}/imgs`, false).then((res) => {
				console.log("PASTA CRIADA :: ", res);
			}).catch((e) => {
				console.log("ERRO AO CRIAR PASTA :: ", e);
			})
		})
	}

	private verifyImgsDownloaded(path: string, img: string): Promise<boolean> {
		return new Promise((resolve, reject) => {
			this.file.checkFile(this.file.cacheDirectory, `${path}/${img}`).then(() => {
				resolve(true);
			}).catch(() => {
				console.log('FILE NÃO EXIST :: ', img);
				reject(false);
			});
		});
	}

	next() {
		this.slides.slideNext();
	}

	prev() {
		this.slides.slidePrev();
	}

	ShowOriginal(dados_referencias) {
		this.lista_referencias = '';
		for (let r = 0; r < dados_referencias.length; r++) {
			if (dados_referencias[r].codigo != '') {
				this.lista_referencias += '<div>' + dados_referencias[r].codigo + '</div>';
			}
		}

		let alert = this.alertCtrl.create({
			title: 'CÓDIGO ORIGINAL',
			subTitle: this.lista_referencias,
			buttons: ['OK']
		});
		alert.present();
	}

	ShowEquivalencias(dados_equivalencias) {
		this.lista_equivalencias = '';
		for (let e = 0; e < dados_equivalencias.length; e++) {
			if (dados_equivalencias[e].codigo != '') {
				this.lista_equivalencias += '<div>' + dados_equivalencias[e].codigo + '</div>';
			}
		}

		let alert = this.alertCtrl.create({
			title: 'EQUIVALÊNCIAS',
			subTitle: this.lista_equivalencias,
			buttons: ['OK']
		});
		alert.present();
	}

	ShowRelacionados(item, montadora, modelo, anos) {
		console.log(anos);

		var dados_anos = anos.split('|');
		this.DetalheProvider.ShowRelacionados(this.empresa_slug, item, '', montadora, modelo, dados_anos[0], dados_anos[dados_anos.length - 1]).then((data) => {
			this.relacionados = data;
			console.log(this.relacionados);
		});
	}

	goDetalheSubitem(produto: any) {
		this.navCtrl.push(DetalhePage, { produto: produto, empresa_slug: this.empresa_slug, empresa_nome: this.empresa_nome, cor: this.cor });
	}

	//NEVEGAÇÃO FOOTER
	goFooterLHomePage() { this.navCtrl.setRoot(HomePage); };
	goFooterFavoritosPage() { this.navCtrl.push(FavoritosPage); };
	goFooterPerfilPage() { this.navCtrl.push(PerfilPage); };

	voltar() {
		this.navCtrl.pop();
	}
}
