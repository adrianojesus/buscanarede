import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { map } from 'rxjs/operators';

import { Http } from '@angular/http';

import { ServidorProvider } from '../../providers/servidor/servidor';

import { HomePage } from '../home/home';
import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { LoginProvider } from '../../providers/login/login';

@IonicPage()
@Component({
	selector: 'page-login',
	templateUrl: 'login.html'
})
export class LoginPage {

	usuario: any;
	nome: string;
	email: string;
	setor: string;
	senha: string;
	confirmarsenha: string;
	show: string;

	constructor(public navCtrl: NavController, public navParams: NavParams, public LoginProvider: LoginProvider, public alertCtrl: AlertController, public http: Http, public ServidorProvider: ServidorProvider, private fb: Facebook, private google: GooglePlus) {}

	ionViewDidLoad() {
		this.show = 'FazerLogin';
	}


	logar(){

		if(this.email == undefined || this.senha == undefined){
			let alert = this.alertCtrl.create({
				title: 'Atenção',
				message: 'Preencha todos os campos',
				buttons: ['OK']
			})
			alert.present();
		}else{


			var dados = JSON.stringify({email: this.email, senha: this.senha});
			
			this.http.post(this.ServidorProvider.urlGet()+'login.php', dados).subscribe(
				data => {
					let user = JSON.parse(data["_body"]);

					if(user.result == 'OK'){

						localStorage.setItem('usuario_logado',  'OK');
						localStorage.setItem('usuario_id',  user.id);
						localStorage.setItem('usuario_email',  user.email);
						localStorage.setItem('usuario_nome',  user.nome);
						localStorage.setItem('usuario_empresa',  user.empresa);
						localStorage.setItem('usuario_slug',  user.slug);
						localStorage.setItem('usuario_imagem',  user.imagem);
						localStorage.setItem('usuario_avatar',  user.avatar);
						localStorage.setItem('usuario_tipo',  user.tipo);

						this.navCtrl.setRoot(HomePage);

					}else if(user.result == 'error'){

						let alert = this.alertCtrl.create({
							title: 'Atenção',
							message: 'Email ou Senha invalidos!',
							buttons: ['OK']
						})
						alert.present();

					}else{

					}

				}
			);

		}

	}

	cadastrar(){

		if(this.nome == undefined || this.email == undefined || this.setor == undefined || this.senha == undefined || this.confirmarsenha == undefined){
			let alert = this.alertCtrl.create({
				title: 'Atenção',
				message: 'Preencha todos os campos',
				buttons: ['OK']
			})
			alert.present();
		}else{

			if(this.senha != this.confirmarsenha){

				let alert = this.alertCtrl.create({
					title: 'Atenção',
					message: 'As senhas não conferem',
					buttons: ['OK']
				})
				alert.present();

			}else{


				var dados = JSON.stringify({nome: this.nome, email: this.email, setor: this.setor, senha: this.senha});

				this.http.post(this.ServidorProvider.urlGet()+'cadastro.php', dados).subscribe(
				data => {
						let user = JSON.parse(data["_body"]);

						if(user.result == 'OK'){

							localStorage.setItem('usuario_logado',  'OK');
							localStorage.setItem('usuario_id',  user.id);
							localStorage.setItem('usuario_email',  user.email);
							localStorage.setItem('usuario_nome',  user.nome);
							localStorage.setItem('usuario_empresa',  user.empresa);
							localStorage.setItem('usuario_slug',  user.slug);
							localStorage.setItem('usuario_imagem',  user.imagem);
							localStorage.setItem('usuario_avatar',  user.avatar);
							localStorage.setItem('usuario_tipo',  user.tipo);

							this.navCtrl.setRoot(HomePage);

						}else if(user.result != 'OK'){

							let alert = this.alertCtrl.create({
								title: 'Atenção',
								message: user.result,
								buttons: ['OK']
							})
							alert.present();

						}else{

						}

					}
				);

			}

		}

	}

	showContent(local){
		this.show = local;
	}


	async loginFacebook(){

		const loginResponse = await this.fb.login(['public_profile', 'user_friends', 'email']);

		this.usuario = await this.fb.api(
			`${loginResponse.authResponse.userID}/?fields=id,name,picture,email`, ['public_profile']
		);

		if(this.usuario.id != ''){

			var dados = JSON.stringify({email: this.usuario.email, id: this.usuario.id, name: this.usuario.name});
			
			this.http.post(this.ServidorProvider.urlGet()+'facebook.php', dados).subscribe(
				data => {
					let user = JSON.parse(data["_body"]);

					if(user.result == 'OK'){

						localStorage.setItem('usuario_logado',  'OK');
						localStorage.setItem('usuario_id',  user.id);
						localStorage.setItem('usuario_email',  user.email);
						localStorage.setItem('usuario_nome',  user.nome);
						localStorage.setItem('usuario_empresa',  user.empresa);
						localStorage.setItem('usuario_slug',  user.slug);
						localStorage.setItem('usuario_imagem',  user.imagem);
						localStorage.setItem('usuario_avatar',  user.avatar);
						localStorage.setItem('usuario_tipo',  user.tipo);

						this.navCtrl.setRoot(HomePage);

					}else if(user.result == 'error'){

						let alert = this.alertCtrl.create({
							title: 'Atenção',
							message: user.result,
							buttons: ['OK']
						})
						alert.present();

					}else{

					}

				}
			);
		}

	}

	loginGoogle(){
	
		this.google.login({})
		.then(res => {

			if(res.email != ''){


				var dados = JSON.stringify({userEmail: res.email, userID: res.userId, userName: res.displayName, userPicture: res.imageUrl});
			
				this.http.post(this.ServidorProvider.urlGet()+'google.php', dados).subscribe(
					data => {
						let user = JSON.parse(data["_body"]);

						if(user.result == 'OK'){

							localStorage.setItem('usuario_logado',  'OK');
							localStorage.setItem('usuario_id',  user.id);
							localStorage.setItem('usuario_email',  user.email);
							localStorage.setItem('usuario_nome',  user.nome);
							localStorage.setItem('usuario_empresa',  user.empresa);
							localStorage.setItem('usuario_slug',  user.slug);
							localStorage.setItem('usuario_imagem',  user.imagem);
							localStorage.setItem('usuario_avatar',  user.avatar);
							localStorage.setItem('usuario_tipo',  user.tipo);

							this.navCtrl.setRoot(HomePage);

						}else if(user.result == 'error'){

							let alert = this.alertCtrl.create({
								title: 'Atenção',
								message: user.result,
								buttons: ['OK']
							})
							alert.present();

						}else{

						}

					}
				);

			}

		})
		.catch(err => {console.error(err)});

	}


}
