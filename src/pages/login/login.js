var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { map } from 'rxjs/operators';
import { Http } from '@angular/http';
import { ServidorProvider } from '../../providers/servidor/servidor';
import { HomePage } from '../home/home';
import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { LoginProvider } from '../../providers/login/login';
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, LoginProvider, alertCtrl, http, ServidorProvider, fb, google) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.LoginProvider = LoginProvider;
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.ServidorProvider = ServidorProvider;
        this.fb = fb;
        this.google = google;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        this.show = 'FazerLogin';
    };
    LoginPage.prototype.logar = function () {
        var _this = this;
        if (this.email == undefined || this.senha == undefined) {
            var alert_1 = this.alertCtrl.create({
                title: 'Atenção',
                message: 'Preencha todos os campos',
                buttons: ['OK']
            });
            alert_1.present();
        }
        else {
            this.http.get(this.ServidorProvider.urlGet() + 'login.php?email=' + this.email + '&senha=' + this.senha + '').pipe(map(function (res) { return res.json(); }))
                .subscribe(function (dados) {
                if (dados.msg.logado == 'sim') {
                    localStorage.setItem('usuario_logado', 'OK');
                    localStorage.setItem('usuario_id', dados.dados.id);
                    localStorage.setItem('usuario_email', dados.dados.email);
                    localStorage.setItem('usuario_nome', dados.dados.nome);
                    localStorage.setItem('usuario_empresa', dados.dados.empresa);
                    localStorage.setItem('usuario_slug', dados.dados.slug);
                    localStorage.setItem('usuario_imagem', dados.dados.imagem);
                    localStorage.setItem('usuario_avatar', dados.dados.avatar);
                    localStorage.setItem('usuario_tipo', dados.dados.tipo);
                    _this.navCtrl.setRoot(HomePage);
                }
                else {
                    var alert_2 = _this.alertCtrl.create({
                        title: 'Atenção',
                        message: dados.msg.texto,
                        buttons: ['OK']
                    });
                    alert_2.present();
                }
            });
        }
    };
    LoginPage.prototype.cadastrar = function () {
        var _this = this;
        if (this.nome == undefined || this.email == undefined || this.setor == undefined || this.senha == undefined || this.confirmarsenha == undefined) {
            var alert_3 = this.alertCtrl.create({
                title: 'Atenção',
                message: 'Preencha todos os campos',
                buttons: ['OK']
            });
            alert_3.present();
        }
        else {
            if (this.senha != this.confirmarsenha) {
                var alert_4 = this.alertCtrl.create({
                    title: 'Atenção',
                    message: 'As senhas não conferem',
                    buttons: ['OK']
                });
                alert_4.present();
            }
            else {
                this.http.get(this.ServidorProvider.urlGet() + 'cadastro.php?nome=' + this.nome + '&email=' + this.email + '&setor=' + this.setor + '&senha=' + this.senha + '').pipe(map(function (res) { return res.json(); }))
                    .subscribe(function (dados) {
                    if (dados.msg.logado == 'sim') {
                        localStorage.setItem('usuario_logado', 'OK');
                        localStorage.setItem('usuario_id', dados.dados.id);
                        localStorage.setItem('usuario_email', dados.dados.email);
                        localStorage.setItem('usuario_nome', dados.dados.nome);
                        localStorage.setItem('usuario_empresa', dados.dados.empresa);
                        localStorage.setItem('usuario_slug', dados.dados.slug);
                        localStorage.setItem('usuario_imagem', dados.dados.imagem);
                        localStorage.setItem('usuario_avatar', dados.dados.avatar);
                        localStorage.setItem('usuario_tipo', dados.dados.tipo);
                        _this.navCtrl.setRoot(HomePage);
                    }
                    else {
                        var alert_5 = _this.alertCtrl.create({
                            title: 'Atenção',
                            message: dados.msg.texto,
                            buttons: ['OK']
                        });
                        alert_5.present();
                    }
                });
            }
        }
    };
    LoginPage.prototype.showContent = function (local) {
        this.show = local;
    };
    LoginPage.prototype.loginFacebook = function () {
        return __awaiter(this, void 0, void 0, function () {
            var loginResponse, _a;
            var _this = this;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, this.fb.login(['public_profile', 'user_friends', 'email'])];
                    case 1:
                        loginResponse = _b.sent();
                        _a = this;
                        return [4 /*yield*/, this.fb.api(loginResponse.authResponse.userID + "/?fields=id,name,picture,email", ['public_profile'])];
                    case 2:
                        _a.usuario = _b.sent();
                        if (this.usuario.id != '') {
                            this.http.get(this.ServidorProvider.urlGet() + 'facebook.php?email=' + this.usuario.email + '&id=' + this.usuario.id + '&name=' + this.usuario.name + '').pipe(map(function (res) { return res.json(); }))
                                .subscribe(function (dados) {
                                if (dados.msg.logado == 'sim') {
                                    localStorage.setItem('usuario_logado', 'OK');
                                    localStorage.setItem('usuario_id', dados.dados.id);
                                    localStorage.setItem('usuario_email', dados.dados.email);
                                    localStorage.setItem('usuario_nome', dados.dados.nome);
                                    localStorage.setItem('usuario_empresa', dados.dados.empresa);
                                    localStorage.setItem('usuario_slug', dados.dados.slug);
                                    localStorage.setItem('usuario_imagem', dados.dados.imagem);
                                    localStorage.setItem('usuario_avatar', dados.dados.avatar);
                                    localStorage.setItem('usuario_tipo', dados.dados.tipo);
                                    _this.navCtrl.setRoot(HomePage);
                                }
                                else {
                                    var alert_6 = _this.alertCtrl.create({
                                        title: 'Atenção',
                                        message: dados.msg.texto,
                                        buttons: ['OK']
                                    });
                                    alert_6.present();
                                }
                            });
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.loginGoogle = function () {
        var _this = this;
        this.google.login({})
            .then(function (res) {
            if (res.email != '') {
                _this.http.get(_this.ServidorProvider.urlGet() + 'google.php?userEmail=' + res.email + '&userID=' + res.userId + '&userName=' + res.displayName + '&userPicture=' + res.imageUrl + '').pipe(map(function (res) { return res.json(); }))
                    .subscribe(function (dados) {
                    if (dados.msg.logado == 'sim') {
                        localStorage.setItem('usuario_logado', 'OK');
                        localStorage.setItem('usuario_id', dados.dados.id);
                        localStorage.setItem('usuario_email', dados.dados.email);
                        localStorage.setItem('usuario_nome', dados.dados.nome);
                        localStorage.setItem('usuario_empresa', dados.dados.empresa);
                        localStorage.setItem('usuario_slug', dados.dados.slug);
                        localStorage.setItem('usuario_imagem', dados.dados.imagem);
                        localStorage.setItem('usuario_avatar', dados.dados.avatar);
                        localStorage.setItem('usuario_tipo', dados.dados.tipo);
                        _this.navCtrl.setRoot(HomePage);
                    }
                    else {
                        var alert_7 = _this.alertCtrl.create({
                            title: 'Atenção',
                            message: dados.msg.texto,
                            buttons: ['OK']
                        });
                        alert_7.present();
                    }
                });
            }
        })
            .catch(function (err) { console.error(err); });
    };
    LoginPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-login',
            templateUrl: 'login.html'
        }),
        __metadata("design:paramtypes", [NavController, NavParams, LoginProvider, AlertController, Http, ServidorProvider, Facebook, GooglePlus])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.js.map