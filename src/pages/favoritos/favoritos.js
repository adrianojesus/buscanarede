var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';
import { HomePage } from '../home/home';
import { PerfilPage } from '../perfil/perfil';
import { ListaPage } from '../lista/lista';
import { NewlistPage } from '../newlist/newlist';
import { ServidorProvider } from '../../providers/servidor/servidor';
import { FavoritosProvider } from '../../providers/favoritos/favoritos';
var FavoritosPage = /** @class */ (function () {
    function FavoritosPage(navCtrl, navParams, ServidorProvider, FavoritosProvider, modalCtrl, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ServidorProvider = ServidorProvider;
        this.FavoritosProvider = FavoritosProvider;
        this.modalCtrl = modalCtrl;
        this.http = http;
        this.headerScrollConfig = { cssProperty: 'margin-top', maxValue: 56 };
        this.footerScrollConfig = { cssProperty: 'margin-bottom', maxValue: 56 };
    }
    FavoritosPage_1 = FavoritosPage;
    FavoritosPage.prototype.ionViewDidLoad = function () {
        if (localStorage.getItem('FavoritosPendente') == 'NO') {
            this.getUserListasList();
        }
        else {
            this.getUserListasAtulizar(localStorage.getItem('usuario_id'));
        }
    };
    //ATUALIZA A LISTA DE LISTAS DO USUARIO LOGADO
    FavoritosPage.prototype.getUserListasAtulizar = function (user) {
        var _this = this;
        this.loadpage = false;
        this.http.get(this.ServidorProvider.urlGet() + 'usuarios_listas.php?user=' + user + '').pipe(map(function (res) { return res.json(); })).subscribe(function (dados) {
            if (dados.msg.carregado == 'sim') {
                _this.startUserListasList();
                localStorage.setItem('FavoritosPendente', 'NO');
            }
        });
    };
    FavoritosPage.prototype.startUserListasList = function () {
        var _this = this;
        this.FavoritosProvider.startUserListasList().then(function (data) {
            _this.getUserListasList();
            _this.loadpage = true;
        });
    };
    //PEGA A LISTA DE ListasS DO USUARIO LOGADO
    FavoritosPage.prototype.getUserListasList = function () {
        var _this = this;
        this.FavoritosProvider.goUserListasList('').then(function (data) {
            _this.userlista = data;
            _this.userlistattoal = _this.userlista.length;
            _this.loadpage = true;
        });
    };
    //CRIAR NOVA LISTA
    FavoritosPage.prototype.OpenModalNewList = function () {
        var modal = this.modalCtrl.create(NewlistPage);
        modal.present();
    };
    //NEVEGAÇÃO FOOTER
    FavoritosPage.prototype.goFooterLHomePage = function () { this.navCtrl.setRoot(HomePage); };
    ;
    FavoritosPage.prototype.goFooterFavoritosPage = function () { this.navCtrl.push(FavoritosPage_1); };
    ;
    FavoritosPage.prototype.goFooterPerfilPage = function () { this.navCtrl.push(PerfilPage); };
    ;
    FavoritosPage.prototype.voltar = function () {
        this.navCtrl.pop();
    };
    //NEVEGA ATE A LISTA DA EMPRESA
    FavoritosPage.prototype.goLista = function (empresa_id, empresa_slug, empresa_nome, cor, total_produtos, lista) {
        this.navCtrl.push(ListaPage, { empresa_id: empresa_id, empresa_slug: empresa_slug, empresa_nome: empresa_nome, cor: cor, total_produtos: total_produtos, lista: lista });
    };
    var FavoritosPage_1;
    FavoritosPage = FavoritosPage_1 = __decorate([
        IonicPage(),
        Component({
            selector: 'page-favoritos',
            templateUrl: 'favoritos.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, ServidorProvider, FavoritosProvider, ModalController, Http])
    ], FavoritosPage);
    return FavoritosPage;
}());
export { FavoritosPage };
//# sourceMappingURL=favoritos.js.map