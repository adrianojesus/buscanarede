import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

import { HomePage } from '../home/home';
import { PerfilPage } from '../perfil/perfil';
import { ListaPage } from '../lista/lista';
import { NewlistPage } from '../newlist/newlist';

import { ServidorProvider } from '../../providers/servidor/servidor';
import { FavoritosProvider } from '../../providers/favoritos/favoritos';
import { NetworkProvider } from '../../providers/network/network';

import { ScrollHideConfig } from '../../directives/scroll-hide/scroll-hide';
import { Observable } from 'rxjs/Rx';

@IonicPage()
@Component({
  selector: 'page-favoritos',
  templateUrl: 'favoritos.html',
})
export class FavoritosPage {

	userlista: any;
	userlistattoal: any;
	loadpage: any;
	hideMenu: any;
	
	isConnected: boolean;
	msgoff: string;

	headerScrollConfig: ScrollHideConfig = { cssProperty: 'margin-top', maxValue: 56 };
	footerScrollConfig: ScrollHideConfig = { cssProperty: 'margin-bottom', maxValue: 56 };

	constructor(
		public networkprovider: NetworkProvider, 
		public navCtrl: NavController, 
		public navParams: NavParams, 
		public ServidorProvider: ServidorProvider, 
		public FavoritosProvider: FavoritosProvider, 
		public modalCtrl: ModalController,
		public http: Http
	) {}

	ionViewDidLoad(){

		// Buscando de há conexão com internet
		this.networkprovider.getIsOnline().subscribe((connecton: boolean) => this.isConnected = connecton);

		if(localStorage.getItem('FavoritosPendente') == 'NO'){
			this.getUserListasList();
		}else{

			if(this.isConnected){
				this.getUserListasAtulizar(localStorage.getItem('usuario_id'));
			}else{
				this.loadpage = true;
				this.hideMenu = true;
				this.msgoff = 'VOCÊ PRECISA DE CONEXÃO COM A INTERNET PARA BAIXAR ESTE CATÁLOGO PELA PRIMEIRA VEZ.';
			}

		}
		
	}


	//ATUALIZA A LISTA DE LISTAS DO USUARIO LOGADO
	getUserListasAtulizar(user){

		this.loadpage = false;

		this.http.get(this.ServidorProvider.urlGet()+'usuarios_listas.php?user='+user+'').pipe( map( res => res.json() ) ).subscribe(dados => {
			if(dados.msg.carregado == 'sim'){
				this.startUserListasList();
				localStorage.setItem('FavoritosPendente', 'NO');
			}
		});

	}

	startUserListasList(){

		this.FavoritosProvider.startUserListasList().then((data) => {
			this.getUserListasList();
			this.loadpage = true;
		});

	}

	//PEGA A LISTA DE LISTA DO USUARIO LOGADO
	getUserListasList(){

		this.FavoritosProvider.goUserListasList('').then((data) => {
			this.userlista = data;
			this.userlistattoal = this.userlista.length;
			this.loadpage = true;
		});

	}

	//CRIAR NOVA LISTA
	OpenModalNewList(){
		const modal = this.modalCtrl.create(NewlistPage);
    	modal.present();
	}



	//NEVEGAÇÃO FOOTER
	goFooterLHomePage() {this.navCtrl.setRoot(HomePage);};
	goFooterFavoritosPage() {this.navCtrl.push(FavoritosPage);};
	goFooterPerfilPage() {this.navCtrl.push(PerfilPage);};

	voltar(){
		this.navCtrl.pop();
	}

	//NEVEGA ATE A LISTA DA EMPRESA
	goLista(empresa_id, empresa_slug, empresa_nome, cor, total_produtos, lista) {
		this.navCtrl.push(ListaPage, {empresa_id: empresa_id, empresa_slug: empresa_slug, empresa_nome: empresa_nome, cor: cor, total_produtos:total_produtos, lista:lista});
	}

}
