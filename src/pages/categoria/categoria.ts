import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CategoriaProvider } from '../../providers/categoria/categoria'; 

import { HomePage } from '../home/home';
import { FavoritosPage } from '../favoritos/favoritos';
import { PerfilPage } from '../perfil/perfil';
import { ListaPage } from '../lista/lista';

import { ScrollHideConfig } from '../../directives/scroll-hide/scroll-hide';

@IonicPage()
@Component({
	selector: 'page-categoria',
	templateUrl: 'categoria.html',
})
 export class CategoriaPage {

 	itens: any;
 	categoria: any;

	headerScrollConfig: ScrollHideConfig = { cssProperty: 'margin-top', maxValue: 34 };
	footerScrollConfig: ScrollHideConfig = { cssProperty: 'margin-bottom', maxValue: 50 };

 	constructor(public navCtrl: NavController, public navParams: NavParams, public CategoriaProvider: CategoriaProvider) {
 		this.categoria = this.navParams.get('categoria');
 	}

 	ionViewDidLoad() {

 		this.CategoriaProvider.getGategoriaLista(this.categoria).then((data) => {
          this.itens = data;
        });

 	}

 	goSearchEmpresa(termo) {
 		this.CategoriaProvider.goSearchEmpresa(this.categoria, termo).then((data) => {
 			this.itens = data;
 		});
 	}

	//NEVEGA ATE A LISTA DA EMPRESA
	goLista(empresa_id, empresa_slug, empresa_nome,  cor, total_produtos) {
		this.navCtrl.push(ListaPage, {empresa_id: empresa_id, empresa_slug: empresa_slug, empresa_nome: empresa_nome, cor: cor, total_produtos:total_produtos});
	}


	//NEVEGAÇÃO FOOTER
	goFooterLHomePage() {this.navCtrl.setRoot(HomePage);};
	goFooterFavoritosPage() {this.navCtrl.push(FavoritosPage);};
	goFooterPerfilPage() {this.navCtrl.push(PerfilPage);};

	voltar(){
		this.navCtrl.pop();
	}

 }
