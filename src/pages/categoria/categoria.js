var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CategoriaProvider } from '../../providers/categoria/categoria';
import { HomePage } from '../home/home';
import { FavoritosPage } from '../favoritos/favoritos';
import { PerfilPage } from '../perfil/perfil';
var CategoriaPage = /** @class */ (function () {
    function CategoriaPage(navCtrl, navParams, CategoriaProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.CategoriaProvider = CategoriaProvider;
        this.headerScrollConfig = { cssProperty: 'margin-top', maxValue: 34 };
        this.footerScrollConfig = { cssProperty: 'margin-bottom', maxValue: 50 };
        this.categoria = this.navParams.get('categoria');
    }
    CategoriaPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.CategoriaProvider.getGategoriaLista(this.categoria).then(function (data) {
            _this.itens = data;
        });
    };
    CategoriaPage.prototype.goSearchEmpresa = function (termo) {
        var _this = this;
        this.CategoriaProvider.goSearchEmpresa(this.categoria, termo).then(function (data) {
            _this.itens = data;
        });
    };
    //NEVEGAÇÃO FOOTER
    CategoriaPage.prototype.goFooterLHomePage = function () { this.navCtrl.setRoot(HomePage); };
    ;
    CategoriaPage.prototype.goFooterFavoritosPage = function () { this.navCtrl.push(FavoritosPage); };
    ;
    CategoriaPage.prototype.goFooterPerfilPage = function () { this.navCtrl.push(PerfilPage); };
    ;
    CategoriaPage.prototype.voltar = function () {
        this.navCtrl.pop();
    };
    CategoriaPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-categoria',
            templateUrl: 'categoria.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, CategoriaProvider])
    ], CategoriaPage);
    return CategoriaPage;
}());
export { CategoriaPage };
//# sourceMappingURL=categoria.js.map